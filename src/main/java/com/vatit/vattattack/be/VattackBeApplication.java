package com.vatit.vattattack.be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;

@EnableCaching
@SpringBootApplication
@PropertySource(value = {"classpath:default.properties","file:override.properties"}, ignoreResourceNotFound = true)
public class VattackBeApplication {
    public static void main(String[] args) {
        SpringApplication.run(VattackBeApplication.class, args);
    }
}
