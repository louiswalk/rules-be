package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtExpenseTypeSubCode;
import com.vatit.vattattack.be.service.VtExpenseTypeSubCodeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_EXPENSE_TYPE_SUB_CODES)
public class ExpenseTypeSubCodeController extends CrudController<VtExpenseTypeSubCode, Integer> {
    private VtExpenseTypeSubCodeService vtExpenseTypeSubCodeService;

    @Autowired
    public ExpenseTypeSubCodeController(VtExpenseTypeSubCodeService vtExpenseTypeCategoryService) {
        this.vtExpenseTypeSubCodeService = vtExpenseTypeCategoryService;
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPE_SUB_CODES)
    Iterable<VtExpenseTypeSubCode> getEntities() {
        return vtExpenseTypeSubCodeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_EXPENSE_TYPE_SUB_CODES_PK)
    VtExpenseTypeSubCode updateEntity(@RequestBody VtExpenseTypeSubCode entity) {
        return vtExpenseTypeSubCodeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_EXPENSE_TYPE_SUB_CODES)
    VtExpenseTypeSubCode addEntity(@RequestBody VtExpenseTypeSubCode entity) {
        return vtExpenseTypeSubCodeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_EXPENSE_TYPE_SUB_CODES_PK)
    void deleteEntity(Integer primaryKey) {
        vtExpenseTypeSubCodeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPE_SUB_CODES_PK)
    Optional<VtExpenseTypeSubCode> getEntityByID(Integer primaryKey) {
        return vtExpenseTypeSubCodeService.getEntityByID(primaryKey);
    }

}