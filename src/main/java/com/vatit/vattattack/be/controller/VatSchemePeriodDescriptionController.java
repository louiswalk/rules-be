package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemePeriodDescription;
import com.vatit.vattattack.be.service.VtVatSchemePeriodDescriptionService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION)
public class VatSchemePeriodDescriptionController extends CrudController<VtVatSchemePeriodDescription, Integer> {
    private VtVatSchemePeriodDescriptionService VtVatSchemePeriodDescriptionService;

    @Autowired
    public VatSchemePeriodDescriptionController(VtVatSchemePeriodDescriptionService VtVatSchemePeriodDescriptionService) {
        this.VtVatSchemePeriodDescriptionService = VtVatSchemePeriodDescriptionService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION)
    Iterable<VtVatSchemePeriodDescription> getEntities() {
        return VtVatSchemePeriodDescriptionService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION_PK)
    VtVatSchemePeriodDescription updateEntity(@RequestBody VtVatSchemePeriodDescription entity) {
        return VtVatSchemePeriodDescriptionService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION)
    VtVatSchemePeriodDescription addEntity(@RequestBody VtVatSchemePeriodDescription entity) {
        return VtVatSchemePeriodDescriptionService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION_PK)
    void deleteEntity(Integer primaryKey) {
        VtVatSchemePeriodDescriptionService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION_PK)
    Optional<VtVatSchemePeriodDescription> getEntityByID(Integer primaryKey) {
        return VtVatSchemePeriodDescriptionService.getEntityByID(primaryKey);
    }

}