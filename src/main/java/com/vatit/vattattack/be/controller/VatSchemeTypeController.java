package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemeType;
import com.vatit.vattattack.be.service.VtVatSchemeTypeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_TYPE)
public class VatSchemeTypeController extends CrudController<VtVatSchemeType, Integer> {
    private VtVatSchemeTypeService vtVatSchemeTypeService;

    @Autowired
    public VatSchemeTypeController(VtVatSchemeTypeService vtVatSchemeTypeService) {
        this.vtVatSchemeTypeService = vtVatSchemeTypeService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_TYPE)
    Iterable<VtVatSchemeType> getEntities() {
        return vtVatSchemeTypeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_TYPE_PK)
    VtVatSchemeType updateEntity(@RequestBody VtVatSchemeType entity) {
        return vtVatSchemeTypeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_TYPE)
    VtVatSchemeType addEntity(@RequestBody VtVatSchemeType entity) {
        return vtVatSchemeTypeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_TYPE_PK)
    void deleteEntity(Integer primaryKey) {
        vtVatSchemeTypeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_TYPE_PK)
    Optional<VtVatSchemeType> getEntityByID(Integer primaryKey) {
        return vtVatSchemeTypeService.getEntityByID(primaryKey);
    }

}