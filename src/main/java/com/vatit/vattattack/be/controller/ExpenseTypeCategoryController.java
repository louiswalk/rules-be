package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtExpenseTypeCategory;
import com.vatit.vattattack.be.service.VtExpenseTypeCategoryService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_EXPENSE_TYPE_CATEGORIES)
public class ExpenseTypeCategoryController extends CrudController<VtExpenseTypeCategory, Integer> {
    private VtExpenseTypeCategoryService vtExpenseTypeCategoryService;

    @Autowired
    public ExpenseTypeCategoryController(VtExpenseTypeCategoryService vtExpenseTypeCategoryService) {
        this.vtExpenseTypeCategoryService = vtExpenseTypeCategoryService;
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPE_CATEGORIES)
    Iterable<VtExpenseTypeCategory> getEntities() {
        return vtExpenseTypeCategoryService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_EXPENSE_TYPE_CATEGORIES_PK)
    VtExpenseTypeCategory updateEntity(@RequestBody VtExpenseTypeCategory entity) {
        return vtExpenseTypeCategoryService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_EXPENSE_TYPE_CATEGORIES)
    VtExpenseTypeCategory addEntity(@RequestBody VtExpenseTypeCategory entity) {
        return vtExpenseTypeCategoryService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_EXPENSE_TYPE_CATEGORIES_PK)
    void deleteEntity(@PathVariable Integer primaryKey) {
        vtExpenseTypeCategoryService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPE_CATEGORIES_PK)
    Optional<VtExpenseTypeCategory> getEntityByID(@PathVariable Integer primaryKey) {
        return vtExpenseTypeCategoryService.getEntityByID(primaryKey);
    }
}