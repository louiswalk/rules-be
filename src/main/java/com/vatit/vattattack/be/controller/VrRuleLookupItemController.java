package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VrRuleLookupItem;
import com.vatit.vattattack.be.service.VrRuleLookupItemService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_RULE_LOOKUP_ITEM)
public class VrRuleLookupItemController extends CrudController<VrRuleLookupItem, String> {
    private VrRuleLookupItemService VrRuleLookupItemService;

    @Autowired
    public VrRuleLookupItemController(VrRuleLookupItemService VrRuleLookupItemService) {
        this.VrRuleLookupItemService = VrRuleLookupItemService;
    }

    @Override
    @GetMapping(Constants.API_RULE_LOOKUP_ITEM)
    Iterable<VrRuleLookupItem> getEntities() {
        return VrRuleLookupItemService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_RULE_LOOKUP_ITEM_PK)
    VrRuleLookupItem updateEntity(@RequestBody VrRuleLookupItem entity) {
        return VrRuleLookupItemService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_RULE_LOOKUP_ITEM)
    VrRuleLookupItem addEntity(@RequestBody VrRuleLookupItem entity) {
        return VrRuleLookupItemService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_RULE_LOOKUP_ITEM_PK)
    void deleteEntity(String primaryKey) {
        VrRuleLookupItemService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_RULE_LOOKUP_ITEM_PK)
    Optional<VrRuleLookupItem> getEntityByID(String primaryKey) {
        return VrRuleLookupItemService.getEntityByID(primaryKey);
    }

}