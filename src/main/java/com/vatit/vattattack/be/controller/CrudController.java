package com.vatit.vattattack.be.controller;

import java.util.Optional;

abstract class CrudController<T, V> {
    abstract Iterable<T> getEntities();

    abstract T updateEntity(T entity);

    abstract T addEntity(T entity);

    abstract void deleteEntity(V primaryKey);

    abstract Optional<T> getEntityByID(V primaryKey);
}
