package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatAuthority;
import com.vatit.vattattack.be.service.VtVatAuthorityService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_AUTHORITIES)
public class VATAuthorityController extends CrudController<VtVatAuthority, String> {
    private VtVatAuthorityService vtVatAuthorityService;

    @Autowired
    public VATAuthorityController(VtVatAuthorityService vtVatAuthorityService) {
        this.vtVatAuthorityService = vtVatAuthorityService;
    }

    @Override
    @GetMapping(Constants.API_VAT_AUTHORITIES)
    Iterable<VtVatAuthority> getEntities() {
        return vtVatAuthorityService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_AUTHORITIES_PK)
    VtVatAuthority updateEntity(@RequestBody VtVatAuthority entity) {
        return vtVatAuthorityService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_AUTHORITIES)
    VtVatAuthority addEntity(@RequestBody VtVatAuthority entity) {
        return vtVatAuthorityService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_AUTHORITIES_PK)
    void deleteEntity(String primaryKey) {
        vtVatAuthorityService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_AUTHORITIES_PK)
    Optional<VtVatAuthority> getEntityByID(String primaryKey) {
        return vtVatAuthorityService.getEntityByID(primaryKey);
    }
}