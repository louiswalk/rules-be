package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtExpenseAgreementVatRate;
import com.vatit.vattattack.be.service.VtExpenseAgreementVatRateService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_EXPENSE_VAT_RATES)
public class ExpenseAgreementVatRateController extends CrudController<VtExpenseAgreementVatRate, String> {
    private VtExpenseAgreementVatRateService vtExpenseAgreementVatRate;

    @Autowired
    public ExpenseAgreementVatRateController(VtExpenseAgreementVatRateService vtExpenseAgreementVatRate) {
        this.vtExpenseAgreementVatRate = vtExpenseAgreementVatRate;
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_VAT_RATES)
    Iterable<VtExpenseAgreementVatRate> getEntities() {
        return vtExpenseAgreementVatRate.getEntities();
    }

    @Override
    @PutMapping(Constants.API_EXPENSE_VAT_RATES_PK)
    VtExpenseAgreementVatRate updateEntity(@RequestBody VtExpenseAgreementVatRate entity) {
        return vtExpenseAgreementVatRate.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_EXPENSE_VAT_RATES)
    VtExpenseAgreementVatRate addEntity(@RequestBody VtExpenseAgreementVatRate entity) {
        return vtExpenseAgreementVatRate.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_EXPENSE_VAT_RATES_PK)
    void deleteEntity(String primaryKey) {
        vtExpenseAgreementVatRate.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_VAT_RATES_PK)
    Optional<VtExpenseAgreementVatRate> getEntityByID(String primaryKey) {
        return vtExpenseAgreementVatRate.getEntityByID(primaryKey);
    }
}