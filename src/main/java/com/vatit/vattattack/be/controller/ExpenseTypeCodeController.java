package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtExpenseTypeCode;
import com.vatit.vattattack.be.service.VtExpenseTypeCodeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_EXPENSE_TYPE_CODES)
public class ExpenseTypeCodeController extends CrudController<VtExpenseTypeCode, Integer> {
    private VtExpenseTypeCodeService vtExpenseTypeCodeService;

    @Autowired
    public ExpenseTypeCodeController(VtExpenseTypeCodeService vtExpenseTypeCategoryService) {
        this.vtExpenseTypeCodeService = vtExpenseTypeCategoryService;
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPE_CODES)
    Iterable<VtExpenseTypeCode> getEntities() {
        return vtExpenseTypeCodeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_EXPENSE_TYPE_CODES_PK)
    VtExpenseTypeCode updateEntity(@RequestBody VtExpenseTypeCode entity) {
        return vtExpenseTypeCodeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_EXPENSE_TYPE_CODES)
    VtExpenseTypeCode addEntity(@RequestBody VtExpenseTypeCode entity) {
        return vtExpenseTypeCodeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_EXPENSE_TYPE_CODES_PK)
    void deleteEntity(Integer primaryKey) {
        vtExpenseTypeCodeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPE_CODES_PK)
    Optional<VtExpenseTypeCode> getEntityByID(Integer primaryKey) {
        return vtExpenseTypeCodeService.getEntityByID(primaryKey);
    }

}