package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemeExpenseAgreement;
import com.vatit.vattattack.be.service.VtVatSchemeExpenseAgreementService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT)
public class VatSchemeExpenseAgreementController extends CrudController<VtVatSchemeExpenseAgreement, String> {
    private VtVatSchemeExpenseAgreementService vtVatSchemeExpenseAgreementService;

    @Autowired
    public VatSchemeExpenseAgreementController(VtVatSchemeExpenseAgreementService vtVatSchemeExpenseAgreementService) {
        this.vtVatSchemeExpenseAgreementService = vtVatSchemeExpenseAgreementService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT)
    Iterable<VtVatSchemeExpenseAgreement> getEntities() {
        return vtVatSchemeExpenseAgreementService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT_PK)
    VtVatSchemeExpenseAgreement updateEntity(@RequestBody VtVatSchemeExpenseAgreement entity) {
        return vtVatSchemeExpenseAgreementService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT)
    VtVatSchemeExpenseAgreement addEntity(@RequestBody VtVatSchemeExpenseAgreement entity) {
        return vtVatSchemeExpenseAgreementService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT_PK)
    void deleteEntity(String primaryKey) {
        vtVatSchemeExpenseAgreementService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT_PK)
    Optional<VtVatSchemeExpenseAgreement> getEntityByID(String primaryKey) {
        return vtVatSchemeExpenseAgreementService.getEntityByID(primaryKey);
    }

}