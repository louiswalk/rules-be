package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.ReNoReciprocity;
import com.vatit.vattattack.be.service.ReNoReciprocityService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_RE_NO_RECIPROCITY)
public class ReNoReciprocityController extends CrudController<ReNoReciprocity, Integer> {
    private ReNoReciprocityService reNoReciprocityService;

    @Autowired
    public ReNoReciprocityController(ReNoReciprocityService reNoReciprocityService) {
        this.reNoReciprocityService = reNoReciprocityService;
    }


    @Override
    @GetMapping(Constants.API_RE_NO_RECIPROCITY)
    Iterable<ReNoReciprocity> getEntities() {
        return reNoReciprocityService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_RE_NO_RECIPROCITY_PK)
    ReNoReciprocity updateEntity(@RequestBody ReNoReciprocity entity) {
        return reNoReciprocityService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_RE_NO_RECIPROCITY)
    ReNoReciprocity addEntity(@RequestBody ReNoReciprocity entity) {
        return reNoReciprocityService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_RE_NO_RECIPROCITY_PK)
    void deleteEntity(Integer primaryKey) {
        reNoReciprocityService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_RE_NO_RECIPROCITY_PK)
    Optional<ReNoReciprocity> getEntityByID(Integer primaryKey) {
        return reNoReciprocityService.getEntityByID(primaryKey);
    }
}