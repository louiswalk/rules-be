package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.SyCurrency;
import com.vatit.vattattack.be.service.SyCurrencyService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_SY_CURRENCY)
public class SyCurrencyController extends CrudController<SyCurrency, String> {
    private SyCurrencyService syCurrencyService;

    @Autowired
    public SyCurrencyController(SyCurrencyService syCurrencyService) {
        this.syCurrencyService = syCurrencyService;
    }

    @Override
    @GetMapping(Constants.API_SY_CURRENCY)
    Iterable<SyCurrency> getEntities() {
        return syCurrencyService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_SY_CURRENCY_PK)
    SyCurrency updateEntity(@RequestBody SyCurrency entity) {
        return syCurrencyService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_SY_CURRENCY)
    SyCurrency addEntity(@RequestBody SyCurrency entity) {
        return syCurrencyService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_SY_CURRENCY_PK)
    void deleteEntity(String primaryKey) {
        syCurrencyService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_SY_CURRENCY_PK)
    Optional<SyCurrency> getEntityByID(String primaryKey) {
        return syCurrencyService.getEntityByID(primaryKey);
    }

}