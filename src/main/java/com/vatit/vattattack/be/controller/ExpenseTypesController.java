package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtExpenseType;
import com.vatit.vattattack.be.service.VtExpenseTypeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_EXPENSE_TYPES)
public class ExpenseTypesController extends CrudController<VtExpenseType, Integer> {
    private VtExpenseTypeService vtExpenseTypeService;

    @Autowired
    public ExpenseTypesController(VtExpenseTypeService vtExpenseTypeService) {
        this.vtExpenseTypeService = vtExpenseTypeService;
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPES)
    Iterable<VtExpenseType> getEntities() {
        return vtExpenseTypeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_EXPENSE_TYPES_PK)
    VtExpenseType updateEntity(@RequestBody VtExpenseType entity) {
        return vtExpenseTypeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_EXPENSE_TYPES)
    VtExpenseType addEntity(@RequestBody VtExpenseType entity) {
        return vtExpenseTypeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_EXPENSE_TYPES_PK)
    void deleteEntity(Integer primaryKey) {
        vtExpenseTypeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_EXPENSE_TYPES_PK)
    Optional<VtExpenseType> getEntityByID(Integer primaryKey) {
        return vtExpenseTypeService.getEntityByID(primaryKey);
    }
}