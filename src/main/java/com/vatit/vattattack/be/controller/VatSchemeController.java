package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatScheme;
import com.vatit.vattattack.be.service.VtVatSchemeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME)
public class VatSchemeController extends CrudController<VtVatScheme, Integer> {
    private VtVatSchemeService vatSchemeService;

    @Autowired
    public VatSchemeController(VtVatSchemeService vatSchemeService) {
        this.vatSchemeService = vatSchemeService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME)
    Iterable<VtVatScheme> getEntities() {
        return vatSchemeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_PK)
    VtVatScheme updateEntity(@RequestBody VtVatScheme entity) {
        return vatSchemeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME)
    VtVatScheme addEntity(@RequestBody VtVatScheme entity) {
        return vatSchemeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_PK)
    void deleteEntity(Integer primaryKey) {
        vatSchemeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PK)
    Optional<VtVatScheme> getEntityByID(Integer primaryKey) {
        return vatSchemeService.getEntityByID(primaryKey);
    }

}