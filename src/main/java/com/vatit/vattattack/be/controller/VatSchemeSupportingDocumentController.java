package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemeSupportingDocument;
import com.vatit.vattattack.be.service.VtVatSchemeSupportingDocumentService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT)
public class VatSchemeSupportingDocumentController extends CrudController<VtVatSchemeSupportingDocument, Integer> {
    private VtVatSchemeSupportingDocumentService VtVatSchemeSupportingDocumentService;

    @Autowired
    public VatSchemeSupportingDocumentController(VtVatSchemeSupportingDocumentService VtVatSchemeSupportingDocumentService) {
        this.VtVatSchemeSupportingDocumentService = VtVatSchemeSupportingDocumentService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT)
    Iterable<VtVatSchemeSupportingDocument> getEntities() {
        return VtVatSchemeSupportingDocumentService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT_PK)
    VtVatSchemeSupportingDocument updateEntity(@RequestBody VtVatSchemeSupportingDocument entity) {
        return VtVatSchemeSupportingDocumentService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT)
    VtVatSchemeSupportingDocument addEntity(@RequestBody VtVatSchemeSupportingDocument entity) {
        return VtVatSchemeSupportingDocumentService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT_PK)
    void deleteEntity(Integer primaryKey) {
        VtVatSchemeSupportingDocumentService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT_PK)
    Optional<VtVatSchemeSupportingDocument> getEntityByID(Integer primaryKey) {
        return VtVatSchemeSupportingDocumentService.getEntityByID(primaryKey);
    }

}