package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.SyCountry;
import com.vatit.vattattack.be.service.SyCountryService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_SY_COUNTRY)
public class SyCountryController extends CrudController<SyCountry, String> {
    private SyCountryService syCountryService;

    @Autowired
    public SyCountryController(SyCountryService syCountryService) {
        this.syCountryService = syCountryService;
    }

    @Override
    @GetMapping(Constants.API_SY_COUNTRY)
    Iterable<SyCountry> getEntities() {
        return syCountryService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_SY_COUNTRY_PK)
    SyCountry updateEntity(@RequestBody SyCountry entity) {
        return syCountryService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_SY_COUNTRY)
    SyCountry addEntity(@RequestBody SyCountry entity) {
        return syCountryService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_SY_COUNTRY_PK)
    void deleteEntity(String primaryKey) {
        syCountryService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_SY_COUNTRY_PK)
    Optional<SyCountry> getEntityByID(String primaryKey) {
        return syCountryService.getEntityByID(primaryKey);
    }

}