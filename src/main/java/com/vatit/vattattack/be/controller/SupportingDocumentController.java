package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtSupportingDocument;
import com.vatit.vattattack.be.service.VtSupportingDocumentService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_SUPPORTING_DOCUMENTS)
public class SupportingDocumentController extends CrudController<VtSupportingDocument, Integer> {
    private VtSupportingDocumentService vtSupportingDocumentService;

    @Autowired
    public SupportingDocumentController(VtSupportingDocumentService vtSupportingDocumentService) {
        this.vtSupportingDocumentService = vtSupportingDocumentService;
    }

    @Override
    @GetMapping(Constants.API_SUPPORTING_DOCUMENTS)
    Iterable<VtSupportingDocument> getEntities() {
        return vtSupportingDocumentService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_SUPPORTING_DOCUMENTS_PK)
    VtSupportingDocument updateEntity(@RequestBody VtSupportingDocument entity) {
        return vtSupportingDocumentService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_SUPPORTING_DOCUMENTS)
    VtSupportingDocument addEntity(@RequestBody VtSupportingDocument entity) {
        return vtSupportingDocumentService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_SUPPORTING_DOCUMENTS_PK)
    void deleteEntity(Integer primaryKey) {
        vtSupportingDocumentService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_SUPPORTING_DOCUMENTS_PK)
    Optional<VtSupportingDocument> getEntityByID(Integer primaryKey) {
        return vtSupportingDocumentService.getEntityByID(primaryKey);
    }
}