package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VrRuleLookupCategory;
import com.vatit.vattattack.be.service.VrRuleLookupCategoryService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_RULES_LOOKUP_CATEGORY)
public class VrRuleLookupCategoryController extends CrudController<VrRuleLookupCategory, String> {
    private VrRuleLookupCategoryService VrRuleLookupCategoryService;

    @Autowired
    public VrRuleLookupCategoryController(VrRuleLookupCategoryService VrRuleLookupCategoryService) {
        this.VrRuleLookupCategoryService = VrRuleLookupCategoryService;
    }

    @Override
    @GetMapping(Constants.API_VAT_RULES_LOOKUP_CATEGORY)
    Iterable<VrRuleLookupCategory> getEntities() {
        return VrRuleLookupCategoryService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_RULES_LOOKUP_CATEGORY_PK)
    VrRuleLookupCategory updateEntity(@RequestBody VrRuleLookupCategory entity) {
        return VrRuleLookupCategoryService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_RULES_LOOKUP_CATEGORY)
    VrRuleLookupCategory addEntity(@RequestBody VrRuleLookupCategory entity) {
        return VrRuleLookupCategoryService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_RULES_LOOKUP_CATEGORY_PK)
    void deleteEntity(String primaryKey) {
        VrRuleLookupCategoryService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_RULES_LOOKUP_CATEGORY_PK)
    Optional<VrRuleLookupCategory> getEntityByID(String primaryKey) {
        return VrRuleLookupCategoryService.getEntityByID(primaryKey);
    }

}