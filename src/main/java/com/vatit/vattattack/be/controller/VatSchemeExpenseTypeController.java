package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemeExpenseType;
import com.vatit.vattattack.be.service.VtVatSchemeExpenseTypeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_EXPENSE_TYPE)
public class VatSchemeExpenseTypeController extends CrudController<VtVatSchemeExpenseType, String> {
    private VtVatSchemeExpenseTypeService vtVatSchemeExpenseTypeService;

    @Autowired
    public VatSchemeExpenseTypeController(VtVatSchemeExpenseTypeService vtVatSchemeExpenseTypeService) {
        this.vtVatSchemeExpenseTypeService = vtVatSchemeExpenseTypeService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_EXPENSE_TYPE)
    Iterable<VtVatSchemeExpenseType> getEntities() {
        return vtVatSchemeExpenseTypeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_EXPENSE_TYPE_PK)
    VtVatSchemeExpenseType updateEntity(@RequestBody VtVatSchemeExpenseType entity) {
        return vtVatSchemeExpenseTypeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_EXPENSE_TYPE)
    VtVatSchemeExpenseType addEntity(@RequestBody VtVatSchemeExpenseType entity) {
        return vtVatSchemeExpenseTypeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_EXPENSE_TYPE_PK)
    void deleteEntity(String primaryKey) {
        vtVatSchemeExpenseTypeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_EXPENSE_TYPE_PK)
    Optional<VtVatSchemeExpenseType> getEntityByID(String primaryKey) {
        return vtVatSchemeExpenseTypeService.getEntityByID(primaryKey);
    }

}