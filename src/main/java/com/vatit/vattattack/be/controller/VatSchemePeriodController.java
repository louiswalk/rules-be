package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemePeriod;
import com.vatit.vattattack.be.service.VtVatSchemePeriodService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_PERIOD)
public class VatSchemePeriodController extends CrudController<VtVatSchemePeriod, Integer> {
    private VtVatSchemePeriodService vtVatSchemePeriodService;

    @Autowired
    public VatSchemePeriodController(VtVatSchemePeriodService vtVatSchemePeriodService) {
        this.vtVatSchemePeriodService = vtVatSchemePeriodService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PERIOD)
    Iterable<VtVatSchemePeriod> getEntities() {
        return vtVatSchemePeriodService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_PERIOD_PK)
    VtVatSchemePeriod updateEntity(@RequestBody VtVatSchemePeriod entity) {
        return vtVatSchemePeriodService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_PERIOD)
    VtVatSchemePeriod addEntity(@RequestBody VtVatSchemePeriod entity) {
        return vtVatSchemePeriodService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_PERIOD_PK)
    void deleteEntity(Integer primaryKey) {
        vtVatSchemePeriodService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PERIOD_PK)
    Optional<VtVatSchemePeriod> getEntityByID(Integer primaryKey) {
        return vtVatSchemePeriodService.getEntityByID(primaryKey);
    }

}