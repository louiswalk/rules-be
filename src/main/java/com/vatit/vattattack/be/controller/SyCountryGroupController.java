package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.SyCountryGroup;
import com.vatit.vattattack.be.service.SyCountryGroupService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_SY_COUNTRY_GROUP)
public class SyCountryGroupController extends CrudController<SyCountryGroup, String> {
    private SyCountryGroupService syCountryGroupService;

    @Autowired
    public SyCountryGroupController(SyCountryGroupService syCountryGroupService) {
        this.syCountryGroupService = syCountryGroupService;
    }

    @Override
    @GetMapping(Constants.API_SY_COUNTRY_GROUP)
    Iterable<SyCountryGroup> getEntities() {
        return syCountryGroupService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_SY_COUNTRY_GROUP_PK)
    SyCountryGroup updateEntity(@RequestBody SyCountryGroup entity) {
        return syCountryGroupService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_SY_COUNTRY_GROUP)
    SyCountryGroup addEntity(@RequestBody SyCountryGroup entity) {
        return syCountryGroupService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_SY_COUNTRY_GROUP_PK)
    void deleteEntity(String primaryKey) {
        syCountryGroupService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_SY_COUNTRY_GROUP_PK)
    Optional<SyCountryGroup> getEntityByID(String primaryKey) {
        return syCountryGroupService.getEntityByID(primaryKey);
    }

}