package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtVatSchemePeriodType;
import com.vatit.vattattack.be.service.VtVatSchemePeriodTypeService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_VAT_SCHEME_PERIOD_TYPE)
public class VatSchemePeriodTypeController extends CrudController<VtVatSchemePeriodType, Integer> {
    private VtVatSchemePeriodTypeService vtVatSchemePeriodTypeService;

    @Autowired
    public VatSchemePeriodTypeController(VtVatSchemePeriodTypeService vtVatSchemePeriodTypeService) {
        this.vtVatSchemePeriodTypeService = vtVatSchemePeriodTypeService;
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PERIOD_TYPE)
    Iterable<VtVatSchemePeriodType> getEntities() {
        return vtVatSchemePeriodTypeService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_VAT_SCHEME_PERIOD_TYPE_PK)
    VtVatSchemePeriodType updateEntity(@RequestBody VtVatSchemePeriodType entity) {
        return vtVatSchemePeriodTypeService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_VAT_SCHEME_PERIOD_TYPE)
    VtVatSchemePeriodType addEntity(@RequestBody VtVatSchemePeriodType entity) {
        return vtVatSchemePeriodTypeService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_VAT_SCHEME_PERIOD_TYPE_PK)
    void deleteEntity(Integer primaryKey) {
        vtVatSchemePeriodTypeService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_VAT_SCHEME_PERIOD_TYPE_PK)
    Optional<VtVatSchemePeriodType> getEntityByID(Integer primaryKey) {
        return vtVatSchemePeriodTypeService.getEntityByID(primaryKey);
    }

}