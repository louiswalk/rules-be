package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.VtInvoiceStatus;
import com.vatit.vattattack.be.service.VtInvoiceStatusService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_INVOICE_STATUS)
public class VtInvoiceStatusController extends CrudController<VtInvoiceStatus, String> {
    private VtInvoiceStatusService VtInvoiceStatusService;

    @Autowired
    public VtInvoiceStatusController(VtInvoiceStatusService VtInvoiceStatusService) {
        this.VtInvoiceStatusService = VtInvoiceStatusService;
    }

    @Override
    @GetMapping(Constants.API_INVOICE_STATUS)
    Iterable<VtInvoiceStatus> getEntities() {
        return VtInvoiceStatusService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_INVOICE_STATUS_PK)
    VtInvoiceStatus updateEntity(@RequestBody VtInvoiceStatus entity) {
        return VtInvoiceStatusService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_INVOICE_STATUS)
    VtInvoiceStatus addEntity(@RequestBody VtInvoiceStatus entity) {
        return VtInvoiceStatusService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_INVOICE_STATUS_PK)
    void deleteEntity(String primaryKey) {
        VtInvoiceStatusService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_INVOICE_STATUS_PK)
    Optional<VtInvoiceStatus> getEntityByID(String primaryKey) {
        return VtInvoiceStatusService.getEntityByID(primaryKey);
    }

}