package com.vatit.vattattack.be.controller;

import com.vatit.vattattack.be.domain.DuGroup;
import com.vatit.vattattack.be.service.DUGroupService;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController(Constants.API_DU_GROUPS)
public class DUGroupController extends CrudController<DuGroup, Integer> {
    private DUGroupService duGroupService;

    @Autowired
    public DUGroupController(DUGroupService duGroupService) {
        this.duGroupService = duGroupService;
    }


    @Override
    @GetMapping(Constants.API_DU_GROUPS)
    Iterable<DuGroup> getEntities() {
        return duGroupService.getEntities();
    }

    @Override
    @PutMapping(Constants.API_DU_GROUPS_PK)
    DuGroup updateEntity(@RequestBody DuGroup entity) {
        return duGroupService.updateEntity(entity);
    }

    @Override
    @PostMapping(Constants.API_DU_GROUPS)
    DuGroup addEntity(@RequestBody DuGroup entity) {
        return duGroupService.addEntity(entity);
    }

    @Override
    @DeleteMapping(Constants.API_DU_GROUPS_PK)
    void deleteEntity(Integer primaryKey) {
        duGroupService.deleteEntity(primaryKey);
    }

    @Override
    @GetMapping(Constants.API_DU_GROUPS_PK)
    Optional<DuGroup> getEntityByID(Integer primaryKey) {
        return duGroupService.getEntityByID(primaryKey);
    }
}