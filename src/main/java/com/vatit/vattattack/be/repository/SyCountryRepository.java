package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.SyCountry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SyCountryRepository extends CrudRepository<SyCountry, String> {

    @Query("SELECT c FROM SyCountry c left join fetch c.syCurrency cc left join fetch c.syCountryGroup cg")
    Iterable<SyCountry> findAllWithQuery();
}
