package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtVatSchemePeriodDescription;
import org.springframework.data.repository.CrudRepository;

public interface VtVatSchemePeriodDescriptionRepository extends CrudRepository<VtVatSchemePeriodDescription, Integer> {
}
