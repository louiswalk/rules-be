package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VrRuleLookupItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VrRuleLookupItemRepository extends CrudRepository<VrRuleLookupItem, String> {

    @Query("SELECT rli FROM VrRuleLookupItem rli join fetch rli.vrRuleLookupCategory lic")
    Iterable<VrRuleLookupItem> findAllWithQuery();
}
