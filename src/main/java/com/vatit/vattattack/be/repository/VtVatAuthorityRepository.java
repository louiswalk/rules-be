package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtVatAuthority;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VtVatAuthorityRepository extends CrudRepository<VtVatAuthority, String> {

    @Query("SELECT va FROM VtVatAuthority  va left join fetch va.syCountry cc left join fetch cc.syCurrency ccc left join fetch cc.syCountryGroup cg")
    Iterable<VtVatAuthority> findAllWithQuery();
}
