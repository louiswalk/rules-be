package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtExpenseTypeSubCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VtExpenseTypeSubCodeRepository extends CrudRepository<VtExpenseTypeSubCode, Integer> {
    @Query("SELECT etsc FROM VtExpenseTypeSubCode  etsc join fetch etsc.vtExpenseTypeCode etc")
    Iterable<VtExpenseTypeSubCode> findAllWithQuery();
}
