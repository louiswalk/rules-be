package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtVatSchemePeriodType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VtVatSchemePeriodTypeRepository extends CrudRepository<VtVatSchemePeriodType, Integer> {
    @Query("select vspt from VtVatSchemePeriodType vspt join fetch vspt.vtVatSchemePeriodDescription vspd")
    Iterable<VtVatSchemePeriodType> findAllWithQuery();
}
