package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtExpenseTypeCategory;
import org.springframework.data.repository.CrudRepository;

public interface VtExpenseTypeCategoryRepository extends CrudRepository<VtExpenseTypeCategory, Integer> {
}
