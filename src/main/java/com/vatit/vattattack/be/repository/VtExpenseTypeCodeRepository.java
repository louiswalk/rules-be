package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtExpenseTypeCode;
import org.springframework.data.repository.CrudRepository;

public interface VtExpenseTypeCodeRepository extends CrudRepository<VtExpenseTypeCode, Integer> {
}
