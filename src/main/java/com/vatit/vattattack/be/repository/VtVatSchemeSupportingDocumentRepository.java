package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtVatSchemeSupportingDocument;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VtVatSchemeSupportingDocumentRepository extends CrudRepository<VtVatSchemeSupportingDocument, Integer> {

    @Query("SELECT vssd FROM VtVatSchemeSupportingDocument  vssd join fetch vssd.vtVatScheme vs join fetch vssd.vtSupportingDocument sd " +
            "left join fetch vs.syCurrency c join fetch vs.vtVatAuthority va left join fetch va.syCountry cc left join fetch cc.syCurrency ccc left join fetch cc.syCountryGroup cg " +
            "join fetch vs.vtVatSchemeType vst left join fetch vs.invoicePeriodUnits ipu left join fetch ipu.vrRuleLookupCategory ipuc left join fetch vs.invoiceDateCompare idc left join fetch idc.vrRuleLookupCategory idcc " +
            "left join fetch vs.vtVatSchemePeriodDescription pdi left join fetch vs.pastDateInvoiceStatus pdis " +
            "left join fetch vs.finaliseImageGrpId figi " +
            "left join fetch vs.submissionImageGrpId sigr left join fetch vs.deadLineFromPeriodEndUnit dlfpeu left join fetch dlfpeu.vrRuleLookupCategory dlfpeuc")
    Iterable<VtVatSchemeSupportingDocument> findAllWithQuery();
}
