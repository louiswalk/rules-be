package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtExpenseType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VtExpenseTypeRepository extends CrudRepository<VtExpenseType, Integer> {

    @Query("SELECT et FROM VtExpenseType  et join fetch et.vtExpenseTypeSubCode etsc join fetch etsc.vtExpenseTypeCode etc join fetch et.vtExpenseTypeCategory etcc")
    Iterable<VtExpenseType> findAllWithQuery();
}
