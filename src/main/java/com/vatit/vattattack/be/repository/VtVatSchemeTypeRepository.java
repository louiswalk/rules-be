package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtVatSchemeType;
import org.springframework.data.repository.CrudRepository;

public interface VtVatSchemeTypeRepository extends CrudRepository<VtVatSchemeType, Integer> {
}
