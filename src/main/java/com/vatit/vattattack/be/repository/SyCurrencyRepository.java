package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.SyCurrency;
import org.springframework.data.repository.CrudRepository;

public interface SyCurrencyRepository extends CrudRepository<SyCurrency, String> {
}
