package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtSupportingDocument;
import org.springframework.data.repository.CrudRepository;

public interface VtSupportingDocumentRepository extends CrudRepository<VtSupportingDocument, Integer> {
}
