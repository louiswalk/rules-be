package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.DuGroup;
import org.springframework.data.repository.CrudRepository;

public interface DuGroupsRepository extends CrudRepository<DuGroup, Integer> {
}
