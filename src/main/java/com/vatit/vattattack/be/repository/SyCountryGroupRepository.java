package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.SyCountryGroup;
import org.springframework.data.repository.CrudRepository;

public interface SyCountryGroupRepository extends CrudRepository<SyCountryGroup, String> {
}
