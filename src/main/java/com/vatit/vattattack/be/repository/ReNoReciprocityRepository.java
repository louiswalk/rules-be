package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.ReNoReciprocity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ReNoReciprocityRepository extends CrudRepository<ReNoReciprocity, Integer> {

    @Query("SELECT rnr FROM ReNoReciprocity  rnr " +
            "join fetch rnr.clientCountry cc left join fetch cc.syCurrency ccc left join fetch cc.syCountryGroup cccg " +
            "join fetch rnr.vaCountry va left join fetch va.syCurrency vac left join fetch va.syCountryGroup vacg ")
    Iterable<ReNoReciprocity> findAllWithQuery();
}
