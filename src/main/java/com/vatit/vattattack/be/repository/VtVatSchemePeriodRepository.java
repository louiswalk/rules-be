package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.VtVatSchemePeriod;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface VtVatSchemePeriodRepository extends CrudRepository<VtVatSchemePeriod, Integer> {

    @Query("SELECT vsp FROM VtVatSchemePeriod vsp left join fetch vsp.vtVatSchemePeriodDescription vspd left join fetch vsp.vtVatSchemePeriodType vspt left join fetch vspt.vtVatSchemePeriodDescription vvspd")
    Iterable<VtVatSchemePeriod> findAllWithQuery();

    @Query("SELECT vsp FROM VtVatSchemePeriod  vsp " +
            "WHERE (vsp.startMonth<=:startMonth AND vsp.endMonth>=:endMonth AND vsp.vatSchemePeriodId=:periodId) " +
            "OR (vsp.startMonth<=:endMonth and vsp.endMonth>=:endMonth AND vsp.vatSchemePeriodId=:periodId) " +
            "OR (vsp.endMonth>=:startMonth and vsp.startMonth<=:endMonth AND vsp.vatSchemePeriodId=:periodId) ")
    Iterable<VtVatSchemePeriod> findInvoiceInfo(@Param("periodId") Integer periodId,@Param("startMonth") Integer startMonth,@Param("endMonth") Integer endMonth);
}
