package com.vatit.vattattack.be.repository;

import com.vatit.vattattack.be.domain.CiEntity;
import org.springframework.data.repository.CrudRepository;

public interface CiEntityRepository extends CrudRepository<CiEntity, String> {
}
