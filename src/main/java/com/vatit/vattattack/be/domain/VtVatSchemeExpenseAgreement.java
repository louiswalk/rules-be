package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemeExpenseAgreement", schema = "dbo", catalog = "Dragon")
public class VtVatSchemeExpenseAgreement {
    private String agreementId;
    private VtVatSchemeExpenseType vtVatSchemeExpenseType;
    private String entityCode;
    private String countryCode;
    private BigDecimal refundPercent;
    private String overRideInvoiceExpRule;
    private Timestamp validFromDate;
    private Timestamp validToDate;
    private String warningComment;
    private BigDecimal minValueForScan;

    @Id
    @Column(name = "AgreementId", nullable = false)
    public String getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(String agreementId) {
        this.agreementId = agreementId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VatSchemeExpenseTypeId", nullable = true)
    public VtVatSchemeExpenseType getVtVatSchemeExpenseType() {
        return vtVatSchemeExpenseType;
    }

    public void setVtVatSchemeExpenseType(VtVatSchemeExpenseType vatSchemeExpenseTypeId) {
        this.vtVatSchemeExpenseType = vatSchemeExpenseTypeId;
    }

    @Basic
    @Column(name = "EntityCode", length = 60)
    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    @Basic
    @Column(name = "CountryCode", length = 3)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Basic
    @Column(name = "RefundPercent", precision = 4)
    public BigDecimal getRefundPercent() {
        return refundPercent;
    }

    public void setRefundPercent(BigDecimal refundPercent) {
        this.refundPercent = refundPercent;
    }

    @Basic
    @Column(name = "OverRideInvoiceExpRule", length = 1)
    public String getOverRideInvoiceExpRule() {
        return overRideInvoiceExpRule;
    }

    public void setOverRideInvoiceExpRule(String overRideInvoiceExpRule) {
        this.overRideInvoiceExpRule = overRideInvoiceExpRule;
    }

    @Basic
    @Column(name = "ValidFromDate")
    public Timestamp getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(Timestamp validFromDate) {
        this.validFromDate = validFromDate;
    }

    @Basic
    @Column(name = "ValidToDate")
    public Timestamp getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(Timestamp validToDate) {
        this.validToDate = validToDate;
    }

    @Basic
    @Column(name = "WarningComment")
    public String getWarningComment() {
        return warningComment;
    }

    public void setWarningComment(String warningComment) {
        this.warningComment = warningComment;
    }

    @Basic
    @Column(name = "MinValueForScan", precision = 4)
    public BigDecimal getMinValueForScan() {
        return minValueForScan;
    }

    public void setMinValueForScan(BigDecimal minValueForScan) {
        this.minValueForScan = minValueForScan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemeExpenseAgreement that = (VtVatSchemeExpenseAgreement) o;
        return Objects.equals(agreementId, that.agreementId) &&
                Objects.equals(vtVatSchemeExpenseType, that.vtVatSchemeExpenseType) &&
                Objects.equals(entityCode, that.entityCode) &&
                Objects.equals(countryCode, that.countryCode) &&
                Objects.equals(refundPercent, that.refundPercent) &&
                Objects.equals(overRideInvoiceExpRule, that.overRideInvoiceExpRule) &&
                Objects.equals(validFromDate, that.validFromDate) &&
                Objects.equals(validToDate, that.validToDate) &&
                Objects.equals(warningComment, that.warningComment) &&
                Objects.equals(minValueForScan, that.minValueForScan);
    }

    @Override
    public int hashCode() {

        return Objects.hash(agreementId, vtVatSchemeExpenseType, entityCode, countryCode, refundPercent, overRideInvoiceExpRule, validFromDate, validToDate, warningComment, minValueForScan);
    }
}
