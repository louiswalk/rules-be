package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VR_RuleLookupItem", schema = "dbo", catalog = "Dragon")
public class VrRuleLookupItem {
    private String lookupItemId;
    private VrRuleLookupCategory vrRuleLookupCategory;
    private String lookupItemCode;
    private String description;
    private String active;

    @Id
    @Column(name = "LookupItemId", nullable = false)
    public String getLookupItemId() {
        return lookupItemId;
    }

    public void setLookupItemId(String lookupItemId) {
        this.lookupItemId = lookupItemId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CategoryCode", nullable = true)
    public VrRuleLookupCategory getVrRuleLookupCategory() {
        return vrRuleLookupCategory;
    }

    public void setVrRuleLookupCategory(VrRuleLookupCategory vrRuleLookupCategory) {
        this.vrRuleLookupCategory = vrRuleLookupCategory;
    }

    @Basic
    @Column(name = "LookupItemCode", nullable = false, length = 3)
    public String getLookupItemCode() {
        return lookupItemCode;
    }

    public void setLookupItemCode(String lookupItemCode) {
        this.lookupItemCode = lookupItemCode;
    }

    @Basic
    @Column(name = "Description", nullable = true, length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Active", nullable = true, length = 1)
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VrRuleLookupItem that = (VrRuleLookupItem) o;
        return Objects.equals(lookupItemId, that.lookupItemId) &&
                Objects.equals(vrRuleLookupCategory, that.vrRuleLookupCategory) &&
                Objects.equals(lookupItemCode, that.lookupItemCode) &&
                Objects.equals(description, that.description) &&
                Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lookupItemId, vrRuleLookupCategory, lookupItemCode, description, active);
    }
}
