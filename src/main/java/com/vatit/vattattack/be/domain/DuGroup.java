package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "DU_Groups", schema = "dbo", catalog = "Dragon")
public class DuGroup {
    private Integer groupId;
    private Integer typeId;
    private String name;
    private String description;
    private String singular;

    @Id
    @Column(name = "GroupID", nullable = false)
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "TypeID")
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Singular", length = 1)
    public String getSingular() {
        return singular;
    }

    public void setSingular(String singular) {
        this.singular = singular;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DuGroup duGroup = (DuGroup) o;
        return Objects.equals(groupId, duGroup.groupId) &&
                Objects.equals(typeId, duGroup.typeId) &&
                Objects.equals(name, duGroup.name) &&
                Objects.equals(description, duGroup.description) &&
                Objects.equals(singular, duGroup.singular);
    }

    @Override
    public int hashCode() {

        return Objects.hash(groupId, typeId, name, description, singular);
    }
}
