package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "VT_VATScheme", schema = "dbo", catalog = "Dragon")
public class VtVatScheme {
    private Integer vatSchemeId;
    private String schemeName;
    private Timestamp schemeYearStart;
    private Timestamp schemeYearEnd;
    private String agentRequired;
    private SyCurrency syCurrency;
    private VtVatAuthority vtVatAuthority;
    private String scriptPrefix;
    private VtVatSchemeType vtVatSchemeType;
    private String invCapEnableInclusiveAmnt;
    private String invCapEnableExcllusiveAmnt;
    private String invCapEnableVatPaid;
    private String quartlySubmission;
    private BigDecimal quarterMinimum;
    private String yearlySubmission;
    private BigDecimal annualMinimum;
    private String imageForFinalise;
    private String imageForSubmission;
    private Integer invoiceValidPeriod;
    private VrRuleLookupItem invoicePeriodUnits;
    private VrRuleLookupItem invoiceDateCompare;
    private VtInvoiceStatus pastDateInvoiceStatus;
    private VtVatSchemePeriodDescription vtVatSchemePeriodDescription;
    private DuGroup finaliseImageGrpId;
    private DuGroup submissionImageGrpId;
    private String invoiceRulesActive;
    private String claimSubmissionRulesActive;
    private Integer deadLineFromPeriodEnd;
    private VrRuleLookupItem deadLineFromPeriodEndUnit;
    private String strictQuarters;
    private String strictAnnual;
    private String invCapEnableExemptAmnt;
    private String invCapEnableNonVat;
    private String invoiceRulesOnClient;
    private String clientPaidDirectly;
    private String shortName;
    private String quarterOverlap;
    private Boolean potentialDefault;
    private String region;
    private Boolean hasRegions;

    @Id
    @Column(name = "VATSchemeID", nullable = false)
    public Integer getVatSchemeId() {
        return vatSchemeId;
    }

    public void setVatSchemeId(Integer vatSchemeId) {
        this.vatSchemeId = vatSchemeId;
    }

    @Basic
    @Column(name = "SchemeName", length = 100)
    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    @Basic
    @Column(name = "SchemeYearStart")
    public Timestamp getSchemeYearStart() {
        return schemeYearStart;
    }

    public void setSchemeYearStart(Timestamp schemeYearStart) {
        this.schemeYearStart = schemeYearStart;
    }

    @Basic
    @Column(name = "SchemeYearEnd")
    public Timestamp getSchemeYearEnd() {
        return schemeYearEnd;
    }

    public void setSchemeYearEnd(Timestamp schemeYearEnd) {
        this.schemeYearEnd = schemeYearEnd;
    }

    @Basic
    @Column(name = "AgentRequired", length = 1)
    public String getAgentRequired() {
        return agentRequired;
    }

    public void setAgentRequired(String agentRequired) {
        this.agentRequired = agentRequired;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CurrencyCode#Operating")
    public SyCurrency getSyCurrency() {
        return syCurrency;
    }

    public void setSyCurrency(SyCurrency currencyCodeOperating) {
        this.syCurrency = currencyCodeOperating;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EntityCode#VatAuthority")
    public VtVatAuthority getVtVatAuthority() {
        return vtVatAuthority;
    }

    public void setVtVatAuthority(VtVatAuthority entityCodeVatAuthority) {
        this.vtVatAuthority = entityCodeVatAuthority;
    }

    @Basic
    @Column(name = "ScriptPrefix", length = 20)
    public String getScriptPrefix() {
        return scriptPrefix;
    }

    public void setScriptPrefix(String scriptPrefix) {
        this.scriptPrefix = scriptPrefix;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VatSchemeTypeID")
    public VtVatSchemeType getVtVatSchemeType() {
        return vtVatSchemeType;
    }

    public void setVtVatSchemeType(VtVatSchemeType vatSchemeTypeId) {
        this.vtVatSchemeType = vatSchemeTypeId;
    }

    @Basic
    @Column(name = "InvCap#EnableInclusiveAmnt", length = 1)
    public String getInvCapEnableInclusiveAmnt() {
        return invCapEnableInclusiveAmnt;
    }

    public void setInvCapEnableInclusiveAmnt(String invCapEnableInclusiveAmnt) {
        this.invCapEnableInclusiveAmnt = invCapEnableInclusiveAmnt;
    }

    @Basic
    @Column(name = "InvCap#EnableExcllusiveAmnt", length = 1)
    public String getInvCapEnableExcllusiveAmnt() {
        return invCapEnableExcllusiveAmnt;
    }

    public void setInvCapEnableExcllusiveAmnt(String invCapEnableExcllusiveAmnt) {
        this.invCapEnableExcllusiveAmnt = invCapEnableExcllusiveAmnt;
    }

    @Basic
    @Column(name = "InvCap#EnableVatPaid", length = 1)
    public String getInvCapEnableVatPaid() {
        return invCapEnableVatPaid;
    }

    public void setInvCapEnableVatPaid(String invCapEnableVatPaid) {
        this.invCapEnableVatPaid = invCapEnableVatPaid;
    }

    @Basic
    @Column(name = "QuartlySubmission", length = 1)
    public String getQuartlySubmission() {
        return quartlySubmission;
    }

    public void setQuartlySubmission(String quartlySubmission) {
        this.quartlySubmission = quartlySubmission;
    }

    @Basic
    @Column(name = "QuarterMinimum", precision = 2)
    public BigDecimal getQuarterMinimum() {
        return quarterMinimum;
    }

    public void setQuarterMinimum(BigDecimal quarterMinimum) {
        this.quarterMinimum = quarterMinimum;
    }

    @Basic
    @Column(name = "YearlySubmission", length = 1)
    public String getYearlySubmission() {
        return yearlySubmission;
    }

    public void setYearlySubmission(String yearlySubmission) {
        this.yearlySubmission = yearlySubmission;
    }

    @Basic
    @Column(name = "AnnualMinimum", precision = 2)
    public BigDecimal getAnnualMinimum() {
        return annualMinimum;
    }

    public void setAnnualMinimum(BigDecimal annualMinimum) {
        this.annualMinimum = annualMinimum;
    }

    @Basic
    @Column(name = "ImageForFinalise", length = 1)
    public String getImageForFinalise() {
        return imageForFinalise;
    }

    public void setImageForFinalise(String imageForFinalise) {
        this.imageForFinalise = imageForFinalise;
    }

    @Basic
    @Column(name = "ImageForSubmission", length = 1)
    public String getImageForSubmission() {
        return imageForSubmission;
    }

    public void setImageForSubmission(String imageForSubmission) {
        this.imageForSubmission = imageForSubmission;
    }

    @Basic
    @Column(name = "InvoiceValidPeriod")
    public Integer getInvoiceValidPeriod() {
        return invoiceValidPeriod;
    }

    public void setInvoiceValidPeriod(Integer invoiceValidPeriod) {
        this.invoiceValidPeriod = invoiceValidPeriod;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "InvoicePeriodUnits")
    public VrRuleLookupItem getInvoicePeriodUnits() {
        return invoicePeriodUnits;
    }

    public void setInvoicePeriodUnits(VrRuleLookupItem invoicePeriodUnits) {
        this.invoicePeriodUnits = invoicePeriodUnits;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "InvoiceDateCompare")
    public VrRuleLookupItem getInvoiceDateCompare() {
        return invoiceDateCompare;
    }

    public void setInvoiceDateCompare(VrRuleLookupItem invoiceDateCompare) {
        this.invoiceDateCompare = invoiceDateCompare;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PastDateInvoiceStatus")
    public VtInvoiceStatus getPastDateInvoiceStatus() {
        return pastDateInvoiceStatus;
    }

    public void setPastDateInvoiceStatus(VtInvoiceStatus pastDateInvoiceStatus) {
        this.pastDateInvoiceStatus = pastDateInvoiceStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PeriodDescriptionID")
    public VtVatSchemePeriodDescription getVtVatSchemePeriodDescription() {
        return vtVatSchemePeriodDescription;
    }

    public void setVtVatSchemePeriodDescription(VtVatSchemePeriodDescription periodDescriptionId) {
        this.vtVatSchemePeriodDescription = periodDescriptionId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FinaliseImageGrpID")
    public DuGroup getFinaliseImageGrpId() {
        return finaliseImageGrpId;
    }

    public void setFinaliseImageGrpId(DuGroup finaliseImageGrpId) {
        this.finaliseImageGrpId = finaliseImageGrpId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SubmissionImageGrpID")
    public DuGroup getSubmissionImageGrpId() {
        return submissionImageGrpId;
    }

    public void setSubmissionImageGrpId(DuGroup submissionImageGrpId) {
        this.submissionImageGrpId = submissionImageGrpId;
    }

    @Basic
    @Column(name = "InvoiceRulesActive", length = 1)
    public String getInvoiceRulesActive() {
        return invoiceRulesActive;
    }

    public void setInvoiceRulesActive(String invoiceRulesActive) {
        this.invoiceRulesActive = invoiceRulesActive;
    }

    @Basic
    @Column(name = "ClaimSubmissionRulesActive", length = 1)
    public String getClaimSubmissionRulesActive() {
        return claimSubmissionRulesActive;
    }

    public void setClaimSubmissionRulesActive(String claimSubmissionRulesActive) {
        this.claimSubmissionRulesActive = claimSubmissionRulesActive;
    }

    @Basic
    @Column(name = "DeadLineFromPeriodEnd")
    public Integer getDeadLineFromPeriodEnd() {
        return deadLineFromPeriodEnd;
    }

    public void setDeadLineFromPeriodEnd(Integer deadLineFromPeriodEnd) {
        this.deadLineFromPeriodEnd = deadLineFromPeriodEnd;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DeadLineFromPeriodEndUnit")
    public VrRuleLookupItem getDeadLineFromPeriodEndUnit() {
        return deadLineFromPeriodEndUnit;
    }

    public void setDeadLineFromPeriodEndUnit(VrRuleLookupItem deadLineFromPeriodEndUnit) {
        this.deadLineFromPeriodEndUnit = deadLineFromPeriodEndUnit;
    }

    @Basic
    @Column(name = "StrictQuarters", length = 1)
    public String getStrictQuarters() {
        return strictQuarters;
    }

    public void setStrictQuarters(String strictQuarters) {
        this.strictQuarters = strictQuarters;
    }

    @Basic
    @Column(name = "StrictAnnual", length = 1)
    public String getStrictAnnual() {
        return strictAnnual;
    }

    public void setStrictAnnual(String strictAnnual) {
        this.strictAnnual = strictAnnual;
    }

    @Basic
    @Column(name = "InvCap#EnableExemptAmnt", length = 1)
    public String getInvCapEnableExemptAmnt() {
        return invCapEnableExemptAmnt;
    }

    public void setInvCapEnableExemptAmnt(String invCapEnableExemptAmnt) {
        this.invCapEnableExemptAmnt = invCapEnableExemptAmnt;
    }

    @Basic
    @Column(name = "InvCap#EnableNonVat", length = 1)
    public String getInvCapEnableNonVat() {
        return invCapEnableNonVat;
    }

    public void setInvCapEnableNonVat(String invCapEnableNonVat) {
        this.invCapEnableNonVat = invCapEnableNonVat;
    }

    @Basic
    @Column(name = "InvoiceRulesOnClient", length = 1)
    public String getInvoiceRulesOnClient() {
        return invoiceRulesOnClient;
    }

    public void setInvoiceRulesOnClient(String invoiceRulesOnClient) {
        this.invoiceRulesOnClient = invoiceRulesOnClient;
    }

    @Basic
    @Column(name = "ClientPaidDirectly", length = 1)
    public String getClientPaidDirectly() {
        return clientPaidDirectly;
    }

    public void setClientPaidDirectly(String clientPaidDirectly) {
        this.clientPaidDirectly = clientPaidDirectly;
    }

    @Basic
    @Column(name = "ShortName", length = 10)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Basic
    @Column(name = "QuarterOverlap", length = 1)
    public String getQuarterOverlap() {
        return quarterOverlap;
    }

    public void setQuarterOverlap(String quarterOverlap) {
        this.quarterOverlap = quarterOverlap;
    }

    @Basic
    @Column(name = "PotentialDefault")
    public Boolean getPotentialDefault() {
        return potentialDefault;
    }

    public void setPotentialDefault(Boolean potentialDefault) {
        this.potentialDefault = potentialDefault;
    }

    @Basic
    @Column(name = "Region", length = 100)
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Basic
    @Column(name = "HasRegions")
    public Boolean getHasRegions() {
        return hasRegions;
    }

    public void setHasRegions(Boolean hasRegions) {
        this.hasRegions = hasRegions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatScheme that = (VtVatScheme) o;
        return Objects.equals(vatSchemeId, that.vatSchemeId) &&
                Objects.equals(schemeName, that.schemeName) &&
                Objects.equals(schemeYearStart, that.schemeYearStart) &&
                Objects.equals(schemeYearEnd, that.schemeYearEnd) &&
                Objects.equals(agentRequired, that.agentRequired) &&
                Objects.equals(syCurrency, that.syCurrency) &&
                Objects.equals(vtVatAuthority, that.vtVatAuthority) &&
                Objects.equals(scriptPrefix, that.scriptPrefix) &&
                Objects.equals(vtVatSchemeType, that.vtVatSchemeType) &&
                Objects.equals(invCapEnableInclusiveAmnt, that.invCapEnableInclusiveAmnt) &&
                Objects.equals(invCapEnableExcllusiveAmnt, that.invCapEnableExcllusiveAmnt) &&
                Objects.equals(invCapEnableVatPaid, that.invCapEnableVatPaid) &&
                Objects.equals(quartlySubmission, that.quartlySubmission) &&
                Objects.equals(quarterMinimum, that.quarterMinimum) &&
                Objects.equals(yearlySubmission, that.yearlySubmission) &&
                Objects.equals(annualMinimum, that.annualMinimum) &&
                Objects.equals(imageForFinalise, that.imageForFinalise) &&
                Objects.equals(imageForSubmission, that.imageForSubmission) &&
                Objects.equals(invoiceValidPeriod, that.invoiceValidPeriod) &&
                Objects.equals(invoicePeriodUnits, that.invoicePeriodUnits) &&
                Objects.equals(invoiceDateCompare, that.invoiceDateCompare) &&
                Objects.equals(pastDateInvoiceStatus, that.pastDateInvoiceStatus) &&
                Objects.equals(vtVatSchemePeriodDescription, that.vtVatSchemePeriodDescription) &&
                Objects.equals(finaliseImageGrpId, that.finaliseImageGrpId) &&
                Objects.equals(submissionImageGrpId, that.submissionImageGrpId) &&
                Objects.equals(invoiceRulesActive, that.invoiceRulesActive) &&
                Objects.equals(claimSubmissionRulesActive, that.claimSubmissionRulesActive) &&
                Objects.equals(deadLineFromPeriodEnd, that.deadLineFromPeriodEnd) &&
                Objects.equals(deadLineFromPeriodEndUnit, that.deadLineFromPeriodEndUnit) &&
                Objects.equals(strictQuarters, that.strictQuarters) &&
                Objects.equals(strictAnnual, that.strictAnnual) &&
                Objects.equals(invCapEnableExemptAmnt, that.invCapEnableExemptAmnt) &&
                Objects.equals(invCapEnableNonVat, that.invCapEnableNonVat) &&
                Objects.equals(invoiceRulesOnClient, that.invoiceRulesOnClient) &&
                Objects.equals(clientPaidDirectly, that.clientPaidDirectly) &&
                Objects.equals(shortName, that.shortName) &&
                Objects.equals(quarterOverlap, that.quarterOverlap) &&
                Objects.equals(potentialDefault, that.potentialDefault) &&
                Objects.equals(region, that.region) &&
                Objects.equals(hasRegions, that.hasRegions);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vatSchemeId, schemeName, schemeYearStart, schemeYearEnd, agentRequired, syCurrency, vtVatAuthority, scriptPrefix, vtVatSchemeType, invCapEnableInclusiveAmnt, invCapEnableExcllusiveAmnt, invCapEnableVatPaid, quartlySubmission, quarterMinimum, yearlySubmission, annualMinimum, imageForFinalise, imageForSubmission, invoiceValidPeriod, invoicePeriodUnits, invoiceDateCompare, pastDateInvoiceStatus, vtVatSchemePeriodDescription, finaliseImageGrpId, submissionImageGrpId, invoiceRulesActive, claimSubmissionRulesActive, deadLineFromPeriodEnd, deadLineFromPeriodEndUnit, strictQuarters, strictAnnual, invCapEnableExemptAmnt, invCapEnableNonVat, invoiceRulesOnClient, clientPaidDirectly, shortName, quarterOverlap, potentialDefault, region, hasRegions);
    }
}
