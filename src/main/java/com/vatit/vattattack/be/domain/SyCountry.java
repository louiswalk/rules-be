package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SY_Country", schema = "dbo", catalog = "Dragon")
public class SyCountry {
    private String countryCode;
    private String description;
    private String internationalDialingCode;
    private String capitalCity;
    private SyCurrency syCurrency;
    private SyCountryGroup syCountryGroup;

    @Id
    @Column(name = "CountryCode", nullable = false, length = -1)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Basic
    @Column(name = "Description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "InternationalDialingCode", length = 5)
    public String getInternationalDialingCode() {
        return internationalDialingCode;
    }

    public void setInternationalDialingCode(String internationalDialingCode) {
        this.internationalDialingCode = internationalDialingCode;
    }

    @Basic
    @Column(name = "CapitalCity")
    public String getCapitalCity() {
        return capitalCity;
    }

    public void setCapitalCity(String capitalCity) {
        this.capitalCity = capitalCity;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CurrencyCode")
    public SyCurrency getSyCurrency() {
        return syCurrency;
    }

    public void setSyCurrency(SyCurrency syCurrency) {
        this.syCurrency = syCurrency;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CountryGroupID")
    public SyCountryGroup getSyCountryGroup() {
        return syCountryGroup;
    }

    public void setSyCountryGroup(SyCountryGroup syCountryGroup) {
        this.syCountryGroup = syCountryGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SyCountry syCountry = (SyCountry) o;
        return Objects.equals(countryCode, syCountry.countryCode) &&
                Objects.equals(description, syCountry.description) &&
                Objects.equals(internationalDialingCode, syCountry.internationalDialingCode) &&
                Objects.equals(capitalCity, syCountry.capitalCity) &&
                Objects.equals(syCurrency, syCountry.syCurrency) &&
                Objects.equals(syCountryGroup, syCountry.syCountryGroup);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryCode, description, internationalDialingCode, capitalCity, syCurrency, syCountryGroup);
    }
}
