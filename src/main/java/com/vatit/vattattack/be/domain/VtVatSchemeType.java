package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemeType", schema = "dbo", catalog = "Dragon")
public class VtVatSchemeType {
    private Integer vatSchemeTypeId;
    private String name;
    private String description;
    private String outputVat;
    private Integer submissionReminderOffset;

    @Id
    @Column(name = "VatSchemeTypeID", nullable = false)
    public Integer getVatSchemeTypeId() {
        return vatSchemeTypeId;
    }

    public void setVatSchemeTypeId(Integer vatSchemeTypeId) {
        this.vatSchemeTypeId = vatSchemeTypeId;
    }

    @Basic
    @Column(name = "Name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description", length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "OutputVat", length = 1)
    public String getOutputVat() {
        return outputVat;
    }

    public void setOutputVat(String outputVat) {
        this.outputVat = outputVat;
    }

    @Basic
    @Column(name = "SubmissionReminderOffset")
    public Integer getSubmissionReminderOffset() {
        return submissionReminderOffset;
    }

    public void setSubmissionReminderOffset(Integer submissionReminderOffset) {
        this.submissionReminderOffset = submissionReminderOffset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemeType that = (VtVatSchemeType) o;
        return Objects.equals(vatSchemeTypeId, that.vatSchemeTypeId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(outputVat, that.outputVat) &&
                Objects.equals(submissionReminderOffset, that.submissionReminderOffset);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vatSchemeTypeId, name, description, outputVat, submissionReminderOffset);
    }
}
