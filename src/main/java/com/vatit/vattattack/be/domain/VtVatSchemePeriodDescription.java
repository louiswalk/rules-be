package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemePeriodDescription", schema = "dbo", catalog = "Dragon")
public class VtVatSchemePeriodDescription {
    private Integer periodDescriptionId;
    private String name;
    private String yearEndDate;

    @Id
    @Column(name = "PeriodDescriptionID", nullable = false)
    public Integer getPeriodDescriptionId() {
        return periodDescriptionId;
    }

    public void setPeriodDescriptionId(Integer periodDescriptionId) {
        this.periodDescriptionId = periodDescriptionId;
    }

    @Basic
    @Column(name = "Name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "YearEndDate", length = 5)
    public String getYearEndDate() {
        return yearEndDate;
    }

    public void setYearEndDate(String yearEndDate) {
        this.yearEndDate = yearEndDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemePeriodDescription that = (VtVatSchemePeriodDescription) o;
        return Objects.equals(periodDescriptionId, that.periodDescriptionId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(yearEndDate, that.yearEndDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(periodDescriptionId, name, yearEndDate);
    }
}
