package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_ExpenseType", schema = "dbo", catalog = "Dragon")
public class VtExpenseType {
    private Integer expenseTypeId;
    private String name;
    private VtExpenseTypeSubCode vtExpenseTypeSubCode;
    private String code;
    private String mandatoryComment;
    private VtExpenseTypeCategory vtExpenseTypeCategory;

    @Id
    @Column(name = "ExpenseTypeID", nullable = false)
    public Integer getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(Integer expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

    @Basic
    @Column(name = "Name", length = 500)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SubCodeID", nullable = true)
    public VtExpenseTypeSubCode getVtExpenseTypeSubCode() {
        return vtExpenseTypeSubCode;
    }

    public void setVtExpenseTypeSubCode(VtExpenseTypeSubCode vtExpenseTypeSubCode) {
        this.vtExpenseTypeSubCode = vtExpenseTypeSubCode;
    }

    @Basic
    @Column(name = "Code", length = 10)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "MandatoryComment", length = 1)
    public String getMandatoryComment() {
        return mandatoryComment;
    }

    public void setMandatoryComment(String mandatoryComment) {
        this.mandatoryComment = mandatoryComment;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ExpenseTypeCategoryID", nullable = true)
    public VtExpenseTypeCategory getVtExpenseTypeCategory() {
        return vtExpenseTypeCategory;
    }

    public void setVtExpenseTypeCategory(VtExpenseTypeCategory expenseTypeCategoryId) {
        this.vtExpenseTypeCategory = expenseTypeCategoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtExpenseType that = (VtExpenseType) o;
        return Objects.equals(expenseTypeId, that.expenseTypeId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(vtExpenseTypeSubCode, that.vtExpenseTypeSubCode) &&
                Objects.equals(code, that.code) &&
                Objects.equals(mandatoryComment, that.mandatoryComment) &&
                Objects.equals(vtExpenseTypeCategory, that.vtExpenseTypeCategory);
    }

    @Override
    public int hashCode() {

        return Objects.hash(expenseTypeId, name, vtExpenseTypeSubCode, code, mandatoryComment, vtExpenseTypeCategory);
    }
}
