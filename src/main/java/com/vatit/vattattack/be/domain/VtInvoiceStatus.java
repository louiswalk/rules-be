package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_InvoiceStatus", schema = "dbo", catalog = "Dragon")
public class VtInvoiceStatus {
    private String invoiceStatusCode;
    private String name;
    private String invoiceCapture;

    @Id
    @Column(name = "InvoiceStatusCode", nullable = false, length = 3)
    public String getInvoiceStatusCode() {
        return invoiceStatusCode;
    }

    public void setInvoiceStatusCode(String invoiceStatusCode) {
        this.invoiceStatusCode = invoiceStatusCode;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "InvoiceCapture", nullable = true, length = 1)
    public String getInvoiceCapture() {
        return invoiceCapture;
    }

    public void setInvoiceCapture(String invoiceCapture) {
        this.invoiceCapture = invoiceCapture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtInvoiceStatus that = (VtInvoiceStatus) o;
        return Objects.equals(invoiceStatusCode, that.invoiceStatusCode) &&
                Objects.equals(name, that.name) &&
                Objects.equals(invoiceCapture, that.invoiceCapture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceStatusCode, name, invoiceCapture);
    }
}
