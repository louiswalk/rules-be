package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemeSupportingDocument", schema = "dbo", catalog = "Dragon")
@IdClass(VtVatSchemeSupportingDocumentPK.class)
public class VtVatSchemeSupportingDocument {
    private VtVatScheme vtVatScheme;
    private VtSupportingDocument vtSupportingDocument;
    private String documentRule;
    private String softRule;
    private String comment;
    private String productStatusLinkId;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VATSchemeID", nullable = true)
    public VtVatScheme getVtVatScheme() {
        return vtVatScheme;
    }

    public void setVtVatScheme(VtVatScheme vtVatScheme) {
        this.vtVatScheme = vtVatScheme;
    }


    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SupportingDocumentID", nullable = true)
    public VtSupportingDocument getVtSupportingDocument() {
        return vtSupportingDocument;
    }

    public void setVtSupportingDocument(VtSupportingDocument vtSupportingDocument) {
        this.vtSupportingDocument = vtSupportingDocument;
    }

    @Basic
    @Column(name = "DocumentRule", length = 3)
    public String getDocumentRule() {
        return documentRule;
    }

    public void setDocumentRule(String documentRule) {
        this.documentRule = documentRule;
    }

    @Basic
    @Column(name = "SoftRule", length = 1)
    public String getSoftRule() {
        return softRule;
    }

    public void setSoftRule(String softRule) {
        this.softRule = softRule;
    }

    @Basic
    @Column(name = "Comment", length = 500)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "ProductStatusLinkID")
    public String getProductStatusLinkId() {
        return productStatusLinkId;
    }

    public void setProductStatusLinkId(String productStatusLinkId) {
        this.productStatusLinkId = productStatusLinkId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemeSupportingDocument that = (VtVatSchemeSupportingDocument) o;
        return Objects.equals(vtVatScheme, that.vtVatScheme) &&
                Objects.equals(vtSupportingDocument, that.vtSupportingDocument) &&
                Objects.equals(documentRule, that.documentRule) &&
                Objects.equals(softRule, that.softRule) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(productStatusLinkId, that.productStatusLinkId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vtVatScheme, vtSupportingDocument, documentRule, softRule, comment, productStatusLinkId);
    }
}
