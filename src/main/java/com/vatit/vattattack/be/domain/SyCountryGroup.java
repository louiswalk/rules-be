package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SY_CountryGroup", schema = "dbo", catalog = "Dragon")
public class SyCountryGroup {
    private Integer countryGroupId;
    private String name;
    private String description;

    @Id
    @Column(name = "CountryGroupID", nullable = false)
    public Integer getCountryGroupId() {
        return countryGroupId;
    }

    public void setCountryGroupId(Integer countryGroupId) {
        this.countryGroupId = countryGroupId;
    }

    @Basic
    @Column(name = "Name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description", nullable = true, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SyCountryGroup that = (SyCountryGroup) o;
        return Objects.equals(countryGroupId, that.countryGroupId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryGroupId, name, description);
    }
}
