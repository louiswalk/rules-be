package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "RE_NoReciprocity", schema = "dbo", catalog = "Dragon")
public class ReNoReciprocity {
    private Integer id;
    private SyCountry clientCountry;
    private SyCountry vaCountry;

    @Id
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VACountry", nullable = true)
    public SyCountry getClientCountry() {
        return clientCountry;
    }

    public void setClientCountry(SyCountry clientCountry) {
        this.clientCountry = clientCountry;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ClientCountry", nullable = true)
    public SyCountry getVaCountry() {
        return vaCountry;
    }

    public void setVaCountry(SyCountry vaCountry) {
        this.vaCountry = vaCountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReNoReciprocity that = (ReNoReciprocity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(clientCountry, that.clientCountry) &&
                Objects.equals(vaCountry, that.vaCountry);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientCountry, vaCountry);
    }
}
