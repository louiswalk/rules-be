package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "VT_ExpenseAgreementVatRate", schema = "dbo", catalog = "Dragon")
public class VtExpenseAgreementVatRate {
    private Integer expenseAgreementVatRateId;
    private VtVatSchemeExpenseType vtVatSchemeExpenseType;
    private BigDecimal rate;
    private Timestamp fromDate;
    private Timestamp toDate;
    private BigDecimal regionalTaxRate;

    @Id
    @Column(name = "ExpenseAgreementVatRateId", nullable = false)
    public Integer getExpenseAgreementVatRateId() {
        return expenseAgreementVatRateId;
    }

    public void setExpenseAgreementVatRateId(Integer expenseAgreementVatRateId) {
        this.expenseAgreementVatRateId = expenseAgreementVatRateId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VatSchemeExpenseTypeId", nullable = true)
    public VtVatSchemeExpenseType getVtVatSchemeExpenseType() {
        return vtVatSchemeExpenseType;
    }

    public void setVtVatSchemeExpenseType(VtVatSchemeExpenseType vatSchemeExpenseType) {
        this.vtVatSchemeExpenseType = vatSchemeExpenseType;
    }

    @Basic
    @Column(name = "Rate", precision = 4)
    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @Basic
    @Column(name = "FromDate")
    public Timestamp getFromDate() {
        return fromDate;
    }

    public void setFromDate(Timestamp fromDate) {
        this.fromDate = fromDate;
    }

    @Basic
    @Column(name = "ToDate")
    public Timestamp getToDate() {
        return toDate;
    }

    public void setToDate(Timestamp toDate) {
        this.toDate = toDate;
    }

    @Basic
    @Column(name = "RegionalTaxRate", precision = 4)
    public BigDecimal getRegionalTaxRate() {
        return regionalTaxRate;
    }

    public void setRegionalTaxRate(BigDecimal regionalTaxRate) {
        this.regionalTaxRate = regionalTaxRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtExpenseAgreementVatRate that = (VtExpenseAgreementVatRate) o;
        return Objects.equals(expenseAgreementVatRateId, that.expenseAgreementVatRateId) &&
                Objects.equals(vtVatSchemeExpenseType, that.vtVatSchemeExpenseType) &&
                Objects.equals(rate, that.rate) &&
                Objects.equals(fromDate, that.fromDate) &&
                Objects.equals(toDate, that.toDate) &&
                Objects.equals(regionalTaxRate, that.regionalTaxRate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(expenseAgreementVatRateId, vtVatSchemeExpenseType, rate, fromDate, toDate, regionalTaxRate);
    }
}
