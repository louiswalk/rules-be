package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemeExpenseType", schema = "dbo", catalog = "Dragon")
public class VtVatSchemeExpenseType {
    private String vatSchemeExpenseTypeId;
    private VtExpenseType vtExpenseType;
    private VtVatScheme vtVatScheme;
    private String active;
    private String zeroRated;

    @Id
    @Column(name = "VatSchemeExpenseTypeId", nullable = false)
    public String getVatSchemeExpenseTypeId() {
        return vatSchemeExpenseTypeId;
    }

    public void setVatSchemeExpenseTypeId(String vatSchemeExpenseTypeId) {
        this.vatSchemeExpenseTypeId = vatSchemeExpenseTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VATSchemeID", nullable = true)
    public VtVatScheme getVtVatScheme() {
        return vtVatScheme;
    }

    public void setVtVatScheme(VtVatScheme vtVatScheme) {
        this.vtVatScheme = vtVatScheme;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ExpenseTypeID", nullable = true)
    public VtExpenseType getVtExpenseType() {
        return vtExpenseType;
    }

    public void setVtExpenseType(VtExpenseType vtExpenseType) {
        this.vtExpenseType = vtExpenseType;
    }

    @Basic
    @Column(name = "Active", length = 1)
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Basic
    @Column(name = "ZeroRated", length = 1)
    public String getZeroRated() {
        return zeroRated;
    }

    public void setZeroRated(String zeroRated) {
        this.zeroRated = zeroRated;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemeExpenseType that = (VtVatSchemeExpenseType) o;
        return Objects.equals(vatSchemeExpenseTypeId, that.vatSchemeExpenseTypeId) &&
                Objects.equals(vtExpenseType, that.vtExpenseType) &&
                Objects.equals(vtVatScheme, that.vtVatScheme) &&
                Objects.equals(active, that.active) &&
                Objects.equals(zeroRated, that.zeroRated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vatSchemeExpenseTypeId, vtExpenseType, vtVatScheme, active, zeroRated);
    }

}
