package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemePeriod", schema = "dbo", catalog = "Dragon")
public class VtVatSchemePeriod {
    private Integer vatSchemePeriodId;
    private VtVatSchemePeriodDescription vtVatSchemePeriodDescription;
    private VtVatSchemePeriodType vtVatSchemePeriodType;
    private Integer startMonth;
    private Integer endMonth;
    private String description;
    private Integer incrementYear;
    private Integer claimYearStartIncrement;

    @Id
    @Column(name = "VatSchemePeriodID", nullable = false)
    public Integer getVatSchemePeriodId() {
        return vatSchemePeriodId;
    }

    public void setVatSchemePeriodId(Integer vatSchemePeriodId) {
        this.vatSchemePeriodId = vatSchemePeriodId;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodDescriptionId", nullable = true)
    public VtVatSchemePeriodDescription getVtVatSchemePeriodDescription() {
        return vtVatSchemePeriodDescription;
    }

    public void setVtVatSchemePeriodDescription(VtVatSchemePeriodDescription periodDescriptionId) {
        this.vtVatSchemePeriodDescription = periodDescriptionId;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vsPeriodTypeId", nullable = true)
    public VtVatSchemePeriodType getVtVatSchemePeriodType() {
        return vtVatSchemePeriodType;
    }

    public void setVtVatSchemePeriodType(VtVatSchemePeriodType vsPeriodTypeId) {
        this.vtVatSchemePeriodType = vsPeriodTypeId;
    }

    @Basic
    @Column(name = "StartMonth")
    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    @Basic
    @Column(name = "EndMonth")
    public Integer getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(Integer endMonth) {
        this.endMonth = endMonth;
    }

    @Basic
    @Column(name = "Description", length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "IncrementYear")
    public Integer getIncrementYear() {
        return incrementYear;
    }

    public void setIncrementYear(Integer incrementYear) {
        this.incrementYear = incrementYear;
    }

    @Basic
    @Column(name = "ClaimYearStartIncrement")
    public Integer getClaimYearStartIncrement() {
        return claimYearStartIncrement;
    }

    public void setClaimYearStartIncrement(Integer claimYearStartIncrement) {
        this.claimYearStartIncrement = claimYearStartIncrement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemePeriod that = (VtVatSchemePeriod) o;
        return Objects.equals(vatSchemePeriodId, that.vatSchemePeriodId) &&
                Objects.equals(vtVatSchemePeriodDescription, that.vtVatSchemePeriodDescription) &&
                Objects.equals(vtVatSchemePeriodType, that.vtVatSchemePeriodType) &&
                Objects.equals(startMonth, that.startMonth) &&
                Objects.equals(endMonth, that.endMonth) &&
                Objects.equals(description, that.description) &&
                Objects.equals(incrementYear, that.incrementYear) &&
                Objects.equals(claimYearStartIncrement, that.claimYearStartIncrement);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vatSchemePeriodId, vtVatSchemePeriodDescription, vtVatSchemePeriodType, startMonth, endMonth, description, incrementYear, claimYearStartIncrement);
    }
}
