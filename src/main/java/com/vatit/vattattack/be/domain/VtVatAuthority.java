package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VATAuthority", schema = "dbo", catalog = "Dragon")
public class VtVatAuthority {
    private String entityCodeVatAuthority;
    private String name;
    private String description;
    private String forExControl;
    private SyCountry syCountry;

    @Id
    @Column(name = "EntityCode#VatAuthority", nullable = false, length = 60)
    public String getEntityCodeVatAuthority() {
        return entityCodeVatAuthority;
    }

    public void setEntityCodeVatAuthority(String entityCodeVatAuthority) {
        this.entityCodeVatAuthority = entityCodeVatAuthority;
    }

    @Basic
    @Column(name = "Name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ForExControl", nullable = false, length = 1)
    public String getForExControl() {
        return forExControl;
    }

    public void setForExControl(String forExControl) {
        this.forExControl = forExControl;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CountryCode", nullable = true)
    public SyCountry getSyCountry() {
        return syCountry;
    }

    public void setSyCountry(SyCountry syCountry) {
        this.syCountry = syCountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatAuthority that = (VtVatAuthority) o;
        return Objects.equals(entityCodeVatAuthority, that.entityCodeVatAuthority) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(forExControl, that.forExControl) &&
                Objects.equals(syCountry, that.syCountry);
    }

    @Override
    public int hashCode() {

        return Objects.hash(entityCodeVatAuthority, name, description, forExControl, syCountry);
    }
}
