package com.vatit.vattattack.be.domain;

import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

public class VtVatSchemeSupportingDocumentPK implements Serializable {
    private VtVatScheme vtVatScheme;
    private VtSupportingDocument vtSupportingDocument;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VATSchemeID", nullable = true)
    public VtVatScheme getVtVatScheme() {
        return vtVatScheme;
    }

    public void setVtVatScheme(VtVatScheme vtVatScheme) {
        this.vtVatScheme = vtVatScheme;
    }


    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SupportingDocumentID", nullable = true)
    public VtSupportingDocument getVtSupportingDocument() {
        return vtSupportingDocument;
    }

    public void setVtSupportingDocument(VtSupportingDocument vtSupportingDocument) {
        this.vtSupportingDocument = vtSupportingDocument;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemeSupportingDocumentPK that = (VtVatSchemeSupportingDocumentPK) o;
        return Objects.equals(vtVatScheme, that.vtVatScheme) &&
                Objects.equals(vtSupportingDocument, that.vtSupportingDocument);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vtVatScheme, vtSupportingDocument);
    }
}
