package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CI_Entity", schema = "dbo", catalog = "Dragon")
public class CiEntity {
    private String entityCode;
    private String entityType;
    private String name;
    private String inactive;
    private String baseType;

    public CiEntity() {
    }

    public CiEntity(String entityCode, String entityType, String name, String inactive, String baseType) {
        this.entityCode = entityCode;
        this.entityType = entityType;
        this.name = name;
        this.inactive = inactive;
        this.baseType = baseType;
    }

    @Id
    @Column(name = "EntityCode", nullable = false)
    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    @Basic
    @Column(name = "EntityType", length = 2)
    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    @Basic
    @Column(name = "Name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Inactive", length = 1)
    public String getInactive() {
        return inactive;
    }

    public void setInactive(String inactive) {
        this.inactive = inactive;
    }

    @Basic
    @Column(name = "BaseType", length = 32)
    public String getBaseType() {
        return baseType;
    }

    public void setBaseType(String baseType) {
        this.baseType = baseType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CiEntity ciEntity = (CiEntity) o;
        return Objects.equals(entityCode, ciEntity.entityCode) &&
                Objects.equals(entityType, ciEntity.entityType) &&
                Objects.equals(name, ciEntity.name) &&
                Objects.equals(inactive, ciEntity.inactive) &&
                Objects.equals(baseType, ciEntity.baseType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(entityCode, entityType, name, inactive, baseType);
    }
}
