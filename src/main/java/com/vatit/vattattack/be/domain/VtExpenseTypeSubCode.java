package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_ExpenseTypeSubCode", schema = "dbo", catalog = "Dragon")
public class VtExpenseTypeSubCode {
    private Integer subCodeId;
    private VtExpenseTypeCode vtExpenseTypeCode;
    private String code;
    private String description;

    @Id
    @Column(name = "SubCodeID", nullable = false)
    public Integer getSubCodeId() {
        return subCodeId;
    }

    public void setSubCodeId(Integer subCodeId) {
        this.subCodeId = subCodeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CodeID")
    public VtExpenseTypeCode getVtExpenseTypeCode() {
        return vtExpenseTypeCode;
    }

    public void setVtExpenseTypeCode(VtExpenseTypeCode codeId) {
        this.vtExpenseTypeCode = codeId;
    }

    @Basic
    @Column(name = "Code", nullable = false, length = 10)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "Description", length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtExpenseTypeSubCode that = (VtExpenseTypeSubCode) o;
        return Objects.equals(subCodeId, that.subCodeId) &&
                Objects.equals(vtExpenseTypeCode, that.vtExpenseTypeCode) &&
                Objects.equals(code, that.code) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(subCodeId, vtExpenseTypeCode, code, description);
    }
}
