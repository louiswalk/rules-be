package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_VatSchemePeriodType", schema = "dbo", catalog = "Dragon")
public class VtVatSchemePeriodType {
    private Integer vsPeriodTypeId;
    private VtVatSchemePeriodDescription vtVatSchemePeriodDescription;
    private String name;
    private String description;
    private String active;
    private Integer sequence;
    private String claimPeriod;
    private String singularPeriod;
    private String earlySubmissionDate;
    private Integer yearIncrementFromStart;

    @Id
    @Column(name = "VSPeriodTypeID", nullable = false)
    public Integer getVsPeriodTypeId() {
        return vsPeriodTypeId;
    }

    public void setVsPeriodTypeId(Integer vsPeriodTypeId) {
        this.vsPeriodTypeId = vsPeriodTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodDescriptionId", nullable = true)
    public VtVatSchemePeriodDescription getVtVatSchemePeriodDescription() {
        return vtVatSchemePeriodDescription;
    }

    public void setVtVatSchemePeriodDescription(VtVatSchemePeriodDescription periodDescriptionId) {
        this.vtVatSchemePeriodDescription = periodDescriptionId;
    }

    @Basic
    @Column(name = "Name", length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description", length = 50)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Active", length = 1)
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Basic
    @Column(name = "Sequence")
    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    @Basic
    @Column(name = "ClaimPeriod", length = 1)
    public String getClaimPeriod() {
        return claimPeriod;
    }

    public void setClaimPeriod(String claimPeriod) {
        this.claimPeriod = claimPeriod;
    }

    @Basic
    @Column(name = "SingularPeriod", length = 1)
    public String getSingularPeriod() {
        return singularPeriod;
    }

    public void setSingularPeriod(String singularPeriod) {
        this.singularPeriod = singularPeriod;
    }

    @Basic
    @Column(name = "EarlySubmissionDate", length = 5)
    public String getEarlySubmissionDate() {
        return earlySubmissionDate;
    }

    public void setEarlySubmissionDate(String earlySubmissionDate) {
        this.earlySubmissionDate = earlySubmissionDate;
    }

    @Basic
    @Column(name = "YearIncrementFromStart")
    public Integer getYearIncrementFromStart() {
        return yearIncrementFromStart;
    }

    public void setYearIncrementFromStart(Integer yearIncrementFromStart) {
        this.yearIncrementFromStart = yearIncrementFromStart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtVatSchemePeriodType that = (VtVatSchemePeriodType) o;
        return Objects.equals(vsPeriodTypeId, that.vsPeriodTypeId) &&
                Objects.equals(vtVatSchemePeriodDescription, that.vtVatSchemePeriodDescription) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(active, that.active) &&
                Objects.equals(sequence, that.sequence) &&
                Objects.equals(claimPeriod, that.claimPeriod) &&
                Objects.equals(singularPeriod, that.singularPeriod) &&
                Objects.equals(earlySubmissionDate, that.earlySubmissionDate) &&
                Objects.equals(yearIncrementFromStart, that.yearIncrementFromStart);
    }

    @Override
    public int hashCode() {

        return Objects.hash(vsPeriodTypeId, vtVatSchemePeriodDescription, name, description, active, sequence, claimPeriod, singularPeriod, earlySubmissionDate, yearIncrementFromStart);
    }
}
