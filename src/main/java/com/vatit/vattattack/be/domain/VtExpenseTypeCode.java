package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_ExpenseTypeCode", schema = "dbo", catalog = "Dragon")
public class VtExpenseTypeCode {
    private Integer codeId;
    private String code;
    private String description;

    @Id
    @Column(name = "CodeID", nullable = false)
    public Integer getCodeId() {
        return codeId;
    }

    public void setCodeId(Integer codeId) {
        this.codeId = codeId;
    }

    @Basic
    @Column(name = "Code", nullable = false, length = 10)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtExpenseTypeCode that = (VtExpenseTypeCode) o;
        return Objects.equals(codeId, that.codeId) &&
                Objects.equals(code, that.code) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(codeId, code, description);
    }
}
