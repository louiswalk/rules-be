package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_SupportingDocument", schema = "dbo", catalog = "Dragon")
public class VtSupportingDocument {
    private Integer supportingDocumentId;
    private String description;
    private String notes;
    private String documentStoreUrl;
    private String mandatoryComment;

    @Id
    @Column(name = "SupportingDocumentID", nullable = false)
    public Integer getSupportingDocumentId() {
        return supportingDocumentId;
    }

    public void setSupportingDocumentId(Integer supportingDocumentId) {
        this.supportingDocumentId = supportingDocumentId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Notes")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "DocumentStoreURL")
    public String getDocumentStoreUrl() {
        return documentStoreUrl;
    }

    public void setDocumentStoreUrl(String documentStoreUrl) {
        this.documentStoreUrl = documentStoreUrl;
    }

    @Basic
    @Column(name = "MandatoryComment", length = 1)
    public String getMandatoryComment() {
        return mandatoryComment;
    }

    public void setMandatoryComment(String mandatoryComment) {
        this.mandatoryComment = mandatoryComment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtSupportingDocument that = (VtSupportingDocument) o;
        return Objects.equals(supportingDocumentId, that.supportingDocumentId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(notes, that.notes) &&
                Objects.equals(documentStoreUrl, that.documentStoreUrl) &&
                Objects.equals(mandatoryComment, that.mandatoryComment);
    }

    @Override
    public int hashCode() {

        return Objects.hash(supportingDocumentId, description, notes, documentStoreUrl, mandatoryComment);
    }
}
