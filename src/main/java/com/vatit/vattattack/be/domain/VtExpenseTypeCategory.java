package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VT_ExpenseTypeCategory", schema = "dbo", catalog = "Dragon")
public class VtExpenseTypeCategory {
    private Integer expenseTypeCategoryId;
    private String description;

    @Id
    @Column(name = "ExpenseTypeCategoryId", nullable = false)
    public Integer getExpenseTypeCategoryId() {
        return expenseTypeCategoryId;
    }

    public void setExpenseTypeCategoryId(Integer expenseTypeCategoryId) {
        this.expenseTypeCategoryId = expenseTypeCategoryId;
    }

    @Basic
    @Column(name = "Description", length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtExpenseTypeCategory that = (VtExpenseTypeCategory) o;
        return Objects.equals(expenseTypeCategoryId, that.expenseTypeCategoryId) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(expenseTypeCategoryId, description);
    }
}
