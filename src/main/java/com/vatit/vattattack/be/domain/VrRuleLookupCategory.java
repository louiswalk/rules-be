package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "VR_RuleLookupCategory", schema = "dbo", catalog = "Dragon")
public class VrRuleLookupCategory {
    private String categoryCode;
    private String description;

    @Id
    @Column(name = "CategoryCode", nullable = false, length = 3)
    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    @Basic
    @Column(name = "Description", nullable = true, length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VrRuleLookupCategory that = (VrRuleLookupCategory) o;
        return Objects.equals(categoryCode, that.categoryCode) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryCode, description);
    }
}
