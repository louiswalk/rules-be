package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SY_Currency", schema = "dbo", catalog = "Dragon")
public class SyCurrency {
    private String currencyCode;
    private String description;
    private Integer numericCode;
    private String symbol;

    @Id
    @Column(name = "CurrencyCode", nullable = false, length = -1)
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Basic
    @Column(name = "Description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "NumericCode", nullable = true)
    public Integer getNumericCode() {
        return numericCode;
    }

    public void setNumericCode(Integer numericCode) {
        this.numericCode = numericCode;
    }

    @Basic
    @Column(name = "Symbol", nullable = true)
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SyCurrency that = (SyCurrency) o;
        return Objects.equals(currencyCode, that.currencyCode) &&
                Objects.equals(description, that.description) &&
                Objects.equals(numericCode, that.numericCode) &&
                Objects.equals(symbol, that.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyCode, description, numericCode, symbol);
    }
}
