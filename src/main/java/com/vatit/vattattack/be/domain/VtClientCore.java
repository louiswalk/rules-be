package com.vatit.vattattack.be.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "VT_ClientCore", schema = "dbo", catalog = "Dragon")
public class VtClientCore {
    private String entityCodeClientCore;
    private String type;
    private Timestamp dateAdded;
    private String natureOfBusiness;
    private String registrationNumber;
    private String directRefund;
    private Timestamp firstContactDate;
    private BigDecimal openingBid;
    private String clientCode;
    private String isValid;
    private BigDecimal openingBidSelfRet;
    private String nextStep;
    private Timestamp nextStepDate;
    private String taxOffice;
    private String vatOffice;
    private Timestamp leadDeadDate;
    private String referral;
    private String statusReason;
    private Timestamp statusDate;
    private Timestamp clientCodeAssignedDate;
    private String clientDocumentsValidated;
    private Timestamp clientDocumentsValidatedDate;
    private String problemClient;
    private String unicodeClientName;
    private String chargeDisbursementFee;
    private String amsLogin;
    private String crmCode;
    private String registeredOnPortal;
    private Timestamp validationDate;
    private BigDecimal referralPcnt;
    private BigDecimal companyPcnt;
    private Timestamp referralEndDate;
    private String portalRegistrationInProcess;
    private Timestamp referralEffiveFromDate;
    private String emailPmntPack;
    private Boolean ratiosActive;
    private String sfid;
    private String name;
    private Boolean isActive;
    private String portalName;
    private Boolean calculateDomesticVat;

    @Id
    @Column(name = "EntityCode#ClientCore", nullable = false, length = 60)
    public String getEntityCodeClientCore() {
        return entityCodeClientCore;
    }

    public void setEntityCodeClientCore(String entityCodeClientCore) {
        this.entityCodeClientCore = entityCodeClientCore;
    }

    @Basic
    @Column(name = "Type", nullable = true, length = 1)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "DateAdded", nullable = false)
    public Timestamp getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    @Basic
    @Column(name = "NatureOfBusiness", nullable = true, length = 255)
    public String getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(String natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    @Basic
    @Column(name = "RegistrationNumber", nullable = true, length = 100)
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Basic
    @Column(name = "DirectRefund", nullable = false, length = 1)
    public String getDirectRefund() {
        return directRefund;
    }

    public void setDirectRefund(String directRefund) {
        this.directRefund = directRefund;
    }

    @Basic
    @Column(name = "FirstContactDate", nullable = true)
    public Timestamp getFirstContactDate() {
        return firstContactDate;
    }

    public void setFirstContactDate(Timestamp firstContactDate) {
        this.firstContactDate = firstContactDate;
    }

    @Basic
    @Column(name = "OpeningBid", nullable = true, precision = 4)
    public BigDecimal getOpeningBid() {
        return openingBid;
    }

    public void setOpeningBid(BigDecimal openingBid) {
        this.openingBid = openingBid;
    }

    @Basic
    @Column(name = "ClientCode", nullable = true, length = 100)
    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    @Basic
    @Column(name = "IsValid", nullable = true, length = 1)
    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    @Basic
    @Column(name = "OpeningBidSelfRet", nullable = true, precision = 4)
    public BigDecimal getOpeningBidSelfRet() {
        return openingBidSelfRet;
    }

    public void setOpeningBidSelfRet(BigDecimal openingBidSelfRet) {
        this.openingBidSelfRet = openingBidSelfRet;
    }

    @Basic
    @Column(name = "NextStep", nullable = true, length = 200)
    public String getNextStep() {
        return nextStep;
    }

    public void setNextStep(String nextStep) {
        this.nextStep = nextStep;
    }

    @Basic
    @Column(name = "NextStepDate", nullable = true)
    public Timestamp getNextStepDate() {
        return nextStepDate;
    }

    public void setNextStepDate(Timestamp nextStepDate) {
        this.nextStepDate = nextStepDate;
    }

    @Basic
    @Column(name = "TaxOffice", nullable = true, length = 100)
    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    @Basic
    @Column(name = "VATOffice", nullable = true, length = 255)
    public String getVatOffice() {
        return vatOffice;
    }

    public void setVatOffice(String vatOffice) {
        this.vatOffice = vatOffice;
    }

    @Basic
    @Column(name = "LeadDeadDate", nullable = true)
    public Timestamp getLeadDeadDate() {
        return leadDeadDate;
    }

    public void setLeadDeadDate(Timestamp leadDeadDate) {
        this.leadDeadDate = leadDeadDate;
    }

    @Basic
    @Column(name = "Referral", nullable = true, length = 255)
    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    @Basic
    @Column(name = "StatusReason", nullable = true, length = 255)
    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    @Basic
    @Column(name = "StatusDate", nullable = true)
    public Timestamp getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Timestamp statusDate) {
        this.statusDate = statusDate;
    }

    @Basic
    @Column(name = "ClientCodeAssignedDate", nullable = true)
    public Timestamp getClientCodeAssignedDate() {
        return clientCodeAssignedDate;
    }

    public void setClientCodeAssignedDate(Timestamp clientCodeAssignedDate) {
        this.clientCodeAssignedDate = clientCodeAssignedDate;
    }

    @Basic
    @Column(name = "ClientDocumentsValidated", nullable = true, length = 1)
    public String getClientDocumentsValidated() {
        return clientDocumentsValidated;
    }

    public void setClientDocumentsValidated(String clientDocumentsValidated) {
        this.clientDocumentsValidated = clientDocumentsValidated;
    }

    @Basic
    @Column(name = "ClientDocumentsValidatedDate", nullable = true)
    public Timestamp getClientDocumentsValidatedDate() {
        return clientDocumentsValidatedDate;
    }

    public void setClientDocumentsValidatedDate(Timestamp clientDocumentsValidatedDate) {
        this.clientDocumentsValidatedDate = clientDocumentsValidatedDate;
    }

    @Basic
    @Column(name = "ProblemClient", nullable = true, length = 1)
    public String getProblemClient() {
        return problemClient;
    }

    public void setProblemClient(String problemClient) {
        this.problemClient = problemClient;
    }

    @Basic
    @Column(name = "Unicode_ClientName", nullable = true, length = 255)
    public String getUnicodeClientName() {
        return unicodeClientName;
    }

    public void setUnicodeClientName(String unicodeClientName) {
        this.unicodeClientName = unicodeClientName;
    }

    @Basic
    @Column(name = "ChargeDisbursementFee", nullable = true, length = 1)
    public String getChargeDisbursementFee() {
        return chargeDisbursementFee;
    }

    public void setChargeDisbursementFee(String chargeDisbursementFee) {
        this.chargeDisbursementFee = chargeDisbursementFee;
    }

    @Basic
    @Column(name = "AMSLogin", nullable = true, length = 100)
    public String getAmsLogin() {
        return amsLogin;
    }

    public void setAmsLogin(String amsLogin) {
        this.amsLogin = amsLogin;
    }

    @Basic
    @Column(name = "CRMCode", nullable = true, length = 20)
    public String getCrmCode() {
        return crmCode;
    }

    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }

    @Basic
    @Column(name = "RegisteredOnPortal", nullable = true, length = 1)
    public String getRegisteredOnPortal() {
        return registeredOnPortal;
    }

    public void setRegisteredOnPortal(String registeredOnPortal) {
        this.registeredOnPortal = registeredOnPortal;
    }

    @Basic
    @Column(name = "ValidationDate", nullable = true)
    public Timestamp getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(Timestamp validationDate) {
        this.validationDate = validationDate;
    }

    @Basic
    @Column(name = "ReferralPcnt", nullable = true, precision = 4)
    public BigDecimal getReferralPcnt() {
        return referralPcnt;
    }

    public void setReferralPcnt(BigDecimal referralPcnt) {
        this.referralPcnt = referralPcnt;
    }

    @Basic
    @Column(name = "CompanyPcnt", nullable = true, precision = 4)
    public BigDecimal getCompanyPcnt() {
        return companyPcnt;
    }

    public void setCompanyPcnt(BigDecimal companyPcnt) {
        this.companyPcnt = companyPcnt;
    }

    @Basic
    @Column(name = "ReferralEndDate", nullable = true)
    public Timestamp getReferralEndDate() {
        return referralEndDate;
    }

    public void setReferralEndDate(Timestamp referralEndDate) {
        this.referralEndDate = referralEndDate;
    }

    @Basic
    @Column(name = "PortalRegistrationInProcess", nullable = true, length = 1)
    public String getPortalRegistrationInProcess() {
        return portalRegistrationInProcess;
    }

    public void setPortalRegistrationInProcess(String portalRegistrationInProcess) {
        this.portalRegistrationInProcess = portalRegistrationInProcess;
    }

    @Basic
    @Column(name = "ReferralEffiveFromDate", nullable = true)
    public Timestamp getReferralEffiveFromDate() {
        return referralEffiveFromDate;
    }

    public void setReferralEffiveFromDate(Timestamp referralEffiveFromDate) {
        this.referralEffiveFromDate = referralEffiveFromDate;
    }

    @Basic
    @Column(name = "EmailPmntPack", nullable = true, length = 1)
    public String getEmailPmntPack() {
        return emailPmntPack;
    }

    public void setEmailPmntPack(String emailPmntPack) {
        this.emailPmntPack = emailPmntPack;
    }

    @Basic
    @Column(name = "RatiosActive", nullable = true)
    public Boolean getRatiosActive() {
        return ratiosActive;
    }

    public void setRatiosActive(Boolean ratiosActive) {
        this.ratiosActive = ratiosActive;
    }

    @Basic
    @Column(name = "SFID", nullable = true, length = 100)
    public String getSfid() {
        return sfid;
    }

    public void setSfid(String sfid) {
        this.sfid = sfid;
    }

    @Basic
    @Column(name = "Name", nullable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "isActive", nullable = true)
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Basic
    @Column(name = "PortalName", nullable = true, length = 255)
    public String getPortalName() {
        return portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    @Basic
    @Column(name = "CalculateDomesticVAT", nullable = true)
    public Boolean getCalculateDomesticVat() {
        return calculateDomesticVat;
    }

    public void setCalculateDomesticVat(Boolean calculateDomesticVat) {
        this.calculateDomesticVat = calculateDomesticVat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VtClientCore that = (VtClientCore) o;
        return Objects.equals(entityCodeClientCore, that.entityCodeClientCore) &&
                Objects.equals(type, that.type) &&
                Objects.equals(dateAdded, that.dateAdded) &&
                Objects.equals(natureOfBusiness, that.natureOfBusiness) &&
                Objects.equals(registrationNumber, that.registrationNumber) &&
                Objects.equals(directRefund, that.directRefund) &&
                Objects.equals(firstContactDate, that.firstContactDate) &&
                Objects.equals(openingBid, that.openingBid) &&
                Objects.equals(clientCode, that.clientCode) &&
                Objects.equals(isValid, that.isValid) &&
                Objects.equals(openingBidSelfRet, that.openingBidSelfRet) &&
                Objects.equals(nextStep, that.nextStep) &&
                Objects.equals(nextStepDate, that.nextStepDate) &&
                Objects.equals(taxOffice, that.taxOffice) &&
                Objects.equals(vatOffice, that.vatOffice) &&
                Objects.equals(leadDeadDate, that.leadDeadDate) &&
                Objects.equals(referral, that.referral) &&
                Objects.equals(statusReason, that.statusReason) &&
                Objects.equals(statusDate, that.statusDate) &&
                Objects.equals(clientCodeAssignedDate, that.clientCodeAssignedDate) &&
                Objects.equals(clientDocumentsValidated, that.clientDocumentsValidated) &&
                Objects.equals(clientDocumentsValidatedDate, that.clientDocumentsValidatedDate) &&
                Objects.equals(problemClient, that.problemClient) &&
                Objects.equals(unicodeClientName, that.unicodeClientName) &&
                Objects.equals(chargeDisbursementFee, that.chargeDisbursementFee) &&
                Objects.equals(amsLogin, that.amsLogin) &&
                Objects.equals(crmCode, that.crmCode) &&
                Objects.equals(registeredOnPortal, that.registeredOnPortal) &&
                Objects.equals(validationDate, that.validationDate) &&
                Objects.equals(referralPcnt, that.referralPcnt) &&
                Objects.equals(companyPcnt, that.companyPcnt) &&
                Objects.equals(referralEndDate, that.referralEndDate) &&
                Objects.equals(portalRegistrationInProcess, that.portalRegistrationInProcess) &&
                Objects.equals(referralEffiveFromDate, that.referralEffiveFromDate) &&
                Objects.equals(emailPmntPack, that.emailPmntPack) &&
                Objects.equals(ratiosActive, that.ratiosActive) &&
                Objects.equals(sfid, that.sfid) &&
                Objects.equals(name, that.name) &&
                Objects.equals(isActive, that.isActive) &&
                Objects.equals(portalName, that.portalName) &&
                Objects.equals(calculateDomesticVat, that.calculateDomesticVat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entityCodeClientCore, type, dateAdded, natureOfBusiness, registrationNumber, directRefund, firstContactDate, openingBid, clientCode, isValid, openingBidSelfRet, nextStep, nextStepDate, taxOffice, vatOffice, leadDeadDate, referral, statusReason, statusDate, clientCodeAssignedDate, clientDocumentsValidated, clientDocumentsValidatedDate, problemClient, unicodeClientName, chargeDisbursementFee, amsLogin, crmCode, registeredOnPortal, validationDate, referralPcnt, companyPcnt, referralEndDate, portalRegistrationInProcess, referralEffiveFromDate, emailPmntPack, ratiosActive, sfid, name, isActive, portalName, calculateDomesticVat);
    }
}
