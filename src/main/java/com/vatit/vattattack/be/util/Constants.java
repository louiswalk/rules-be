package com.vatit.vattattack.be.util;

public class Constants {
    //Insert new vat authority requires following constants
    public static final String TABLE_VatAuthority = "VT_VatAuthority";
    public static final String EntityType_Other = "EO";
    public static final String FALSE_CHAR = "F";

    //API end points
    public static final String API_EXPENSE_TYPE_CATEGORIES = "expense-type-categories";
    public static final String API_EXPENSE_TYPE_CODES = "expense-type-codes";
    public static final String API_EXPENSE_TYPE_SUB_CODES = "expense-type-sub-codes";
    public static final String API_EXPENSE_TYPES = "expense-types";
    public static final String API_EXPENSE_VAT_RATES = "expense-vat-rates";
    public static final String API_VAT_AUTHORITIES = "vat-authorities";
    public static final String API_SUPPORTING_DOCUMENTS = "supporting-documents";
    public static final String API_DU_GROUPS = "du-groups";
    public static final String API_VAT_SCHEME_EXPENSE_AGREEMENT = "vat-scheme-expense-agreements";
    public static final String API_VAT_SCHEME_EXPENSE_TYPE = "vat-scheme-expense-types";
    public static final String API_VAT_SCHEME_PERIOD = "vat-scheme-periods";
    public static final String API_VAT_SCHEME_PERIOD_DESCRIPTION = "vat-scheme-period-descriptions";
    public static final String API_VAT_SCHEME_PERIOD_TYPE = "vat-scheme-period-types";
    public static final String API_VAT_SCHEME_SUPPORTING_DOCUMENT = "vat-scheme-supporting-documents";
    public static final String API_VAT_SCHEME_TYPE = "vat-scheme-types";
    public static final String API_VAT_SCHEME = "vat-schemes";
    public static final String API_RE_NO_RECIPROCITY = "reciprocity";
    public static final String API_SY_COUNTRY = "countries";
    public static final String API_SY_COUNTRY_GROUP = "country-groups";
    public static final String API_SY_CURRENCY = "currencies";
    public static final String API_INVOICE_STATUS = "invoice-statuses";
    public static final String API_VAT_RULES_LOOKUP_CATEGORY = "vat-rule-lookup-categories";
    public static final String API_RULE_LOOKUP_ITEM = "vat-rule-lookup-items";

    //API end points with primary key
    public static final String API_EXPENSE_TYPE_CATEGORIES_PK = "expense-type-categories/{primaryKey}";
    public static final String API_EXPENSE_TYPE_CODES_PK = "expense-type-codes/{primaryKey}";
    public static final String API_EXPENSE_TYPE_SUB_CODES_PK = "expense-type-sub-codes/{primaryKey}";
    public static final String API_EXPENSE_TYPES_PK = "expense-types/{primaryKey}";
    public static final String API_EXPENSE_VAT_RATES_PK = "expense-vat-rates/{primaryKey}";
    public static final String API_VAT_AUTHORITIES_PK = "vat-authorities/{primaryKey}";
    public static final String API_SUPPORTING_DOCUMENTS_PK = "supporting-documents/{primaryKey}";
    public static final String API_DU_GROUPS_PK = "du-groups/{primaryKey}";
    public static final String API_VAT_SCHEME_EXPENSE_AGREEMENT_PK = "vat-scheme-expense-agreements/{primaryKey}";
    public static final String API_VAT_SCHEME_EXPENSE_TYPE_PK = "vat-scheme-expense-types/{primaryKey}";
    public static final String API_VAT_SCHEME_PERIOD_PK = "vat-scheme-periods/{primaryKey}";
    public static final String API_VAT_SCHEME_PERIOD_DESCRIPTION_PK = "vat-scheme-period-descriptions/{primaryKey}";
    public static final String API_VAT_SCHEME_PERIOD_TYPE_PK = "vat-scheme-period-types/{primaryKey}";
    public static final String API_VAT_SCHEME_SUPPORTING_DOCUMENT_PK = "vat-scheme-supporting-documents/{primaryKey}";
    public static final String API_VAT_SCHEME_TYPE_PK = "vat-scheme-types/{primaryKey}";
    public static final String API_VAT_SCHEME_PK = "vat-schemes/{primaryKey}";
    public static final String API_RE_NO_RECIPROCITY_PK = "reciprocity/{primaryKey}";
    public static final String API_SY_COUNTRY_PK = "countries/{primaryKey}";
    public static final String API_SY_COUNTRY_GROUP_PK = "country-groups/{primaryKey}";
    public static final String API_SY_CURRENCY_PK = "currencies/{primaryKey}";
    public static final String API_INVOICE_STATUS_PK = "invoice-statuses/{primaryKey}";
    public static final String API_VAT_RULES_LOOKUP_CATEGORY_PK = "vat-rule-lookup-categories/{primaryKey}";
    public static final String API_RULE_LOOKUP_ITEM_PK = "vat-rule-lookup-items/{primaryKey}";
}
