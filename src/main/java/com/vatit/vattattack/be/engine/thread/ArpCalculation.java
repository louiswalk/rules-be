package com.vatit.vattattack.be.engine.thread;

import com.vatit.vattattack.be.domain.*;
import com.vatit.vattattack.be.engine.domain.calculationrequest.Invoice;
import com.vatit.vattattack.be.engine.service.CalculationService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class ArpCalculation implements Callable<ArrayList<Invoice>> {
    private List<Invoice> invoices;
    private final ArrayList<VtVatScheme> vtVatSchemes;
    private final ArrayList<VtVatSchemeExpenseAgreement> vtVatSchemeExpenseAgreements;
    private final ArrayList<VtExpenseAgreementVatRate> vtExpenseAgreementVatRates;
    private final ArrayList<VtVatSchemeExpenseType> vtVatSchemeExpenseTypes;
    private final List<ReNoReciprocity> reNoReciprocities;
    private CalculationService calculationService;


    public ArpCalculation(List<Invoice> invoices, ArrayList<VtVatScheme> vtVatSchemes, ArrayList<VtVatSchemeExpenseAgreement> vtVatSchemeExpenseAgreements, ArrayList<VtExpenseAgreementVatRate> vtExpenseAgreementVatRates, ArrayList<VtVatSchemeExpenseType> vtVatSchemeExpenseTypes, List<ReNoReciprocity> reNoReciprocities, CalculationService calculationService) {
        this.invoices = invoices;
        this.vtVatSchemes = vtVatSchemes;
        this.vtVatSchemeExpenseAgreements = vtVatSchemeExpenseAgreements;
        this.vtExpenseAgreementVatRates = vtExpenseAgreementVatRates;
        this.vtVatSchemeExpenseTypes = vtVatSchemeExpenseTypes;
        this.reNoReciprocities = reNoReciprocities;
        this.calculationService = calculationService;
    }

    @Override
    public ArrayList<Invoice> call() {
        ArrayList<Invoice> arrayList = new ArrayList<>(invoices);
        return calculationService.calculateARP(arrayList,vtVatSchemes,vtVatSchemeExpenseAgreements,vtExpenseAgreementVatRates,vtVatSchemeExpenseTypes,reNoReciprocities);
    }
}
