package com.vatit.vattattack.be.engine.domain.calculationrequest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vatit.vattattack.be.domain.SyCountry;
import com.vatit.vattattack.be.domain.VtVatScheme;

import java.util.ArrayList;
import java.util.List;

public class Invoice {
    private String externalId;
    private Integer dragonId;
    private String id;
    private double amount;
    private String date;
    private String currencyCode;
    private Connection connection;
    private String countryCode;
    private String clientCountryCode;
    private int vatSchemeID;
    private double arp;
    @JsonIgnore
    private SyCountry clientCountry;
    @JsonIgnore
    private SyCountry expenseCountry;
    @JsonIgnore
    private String vatSchemeExpenseTypeId;
    @JsonIgnore
    private double vatSchemeExpenseAgreementRate;
    @JsonIgnore
    private double vatSchemeVatRate;
    private boolean domesticVat;
    private VtVatScheme vtVatScheme;
    private boolean validInvoice;
    private List<String> calculationErrors;

    public VtVatScheme getVtVatScheme() {
        return vtVatScheme;
    }

    public void setVtVatScheme(VtVatScheme vtVatScheme) {
        this.vtVatScheme = vtVatScheme;
    }

    public boolean isValidInvoice() {
        return validInvoice;
    }

    public void setValidInvoice(boolean validInvoice) {
        this.validInvoice = validInvoice;
    }

    public boolean isDomesticVat() {
        return domesticVat;
    }

    public void setDomesticVat(boolean domesticVat) {
        this.domesticVat = domesticVat;
    }

    public double getVatSchemeVatRate() {
        return vatSchemeVatRate;
    }

    public void setVatSchemeVatRate(double vatSchemeVatRate) {
        this.vatSchemeVatRate = vatSchemeVatRate;
    }

    public double getVatSchemeExpenseAgreementRate() {
        return vatSchemeExpenseAgreementRate;
    }

    public void setVatSchemeExpenseAgreementRate(double vatSchemeExpenseAgreementRate) {
        this.vatSchemeExpenseAgreementRate = vatSchemeExpenseAgreementRate;
    }

    public String getVatSchemeExpenseTypeId() {
        return vatSchemeExpenseTypeId;
    }

    public void setVatSchemeExpenseTypeId(String vatSchemeExpenseTypeId) {
        this.vatSchemeExpenseTypeId = vatSchemeExpenseTypeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDragonId() {
        return dragonId;
    }

    public void setDragonId(Integer dragonId) {
        this.dragonId = dragonId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SyCountry getClientCountry() {
        return clientCountry;
    }

    public void setClientCountry(SyCountry clientCountry) {
        this.clientCountry = clientCountry;
    }

    public SyCountry getExpenseCountry() {
        return expenseCountry;
    }

    public void setExpenseCountry(SyCountry expenseCountry) {
        this.expenseCountry = expenseCountry;
    }

    public String getClientCountryCode() {
        return clientCountryCode;
    }

    public void setClientCountryCode(String clientCountryCode) {
        this.clientCountryCode = clientCountryCode;
    }

    public int getVatSchemeID() {
        return vatSchemeID;
    }

    public void setVatSchemeID(int vatSchemeID) {
        this.vatSchemeID = vatSchemeID;
    }

    public double getArp() {
        return arp;
    }

    public void setArp(double arp) {
        this.arp = arp;
    }

    public List<String> getCalculationErrors() {
        if (calculationErrors == null)
            calculationErrors = new ArrayList<>();
        return calculationErrors;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
