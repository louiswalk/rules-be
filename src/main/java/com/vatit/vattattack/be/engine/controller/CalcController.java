package com.vatit.vattattack.be.engine.controller;

import com.vatit.vattattack.be.engine.domain.calculationrequest.Invoice;
import com.vatit.vattattack.be.engine.service.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class CalcController {
    private CalculationService calculationService;

    @Autowired
    public CalcController(CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    @PostMapping("calc")
    public ArrayList<Invoice> calculateARP(@RequestBody ArrayList<Invoice> initialRequest) {


        System.out.println("Incoming " + initialRequest.size());
        ArrayList<Invoice> jobs = calculationService.initialiseArpCalculation(initialRequest);
        System.out.println("Outgoing " + jobs.size());
        return jobs;
    }
}
