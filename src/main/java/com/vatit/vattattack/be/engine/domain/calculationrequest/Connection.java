package com.vatit.vattattack.be.engine.domain.calculationrequest;

public class Connection {
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
