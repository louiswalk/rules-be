package com.vatit.vattattack.be.engine.domain.calculationrequest;

import java.util.List;

public class InitialRequest {
    private List<Invoice> items;

    public List<Invoice> getItems() {
        return items;
    }

    public void setItems(List<Invoice> items) {
        this.items = items;
    }


}



