package com.vatit.vattattack.be.engine.domain.calculationrequest;

public class Client {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
