package com.vatit.vattattack.be.engine.service;

import com.vatit.vattattack.be.domain.*;
import com.vatit.vattattack.be.engine.domain.calculationrequest.Invoice;
import com.vatit.vattattack.be.engine.thread.ArpCalculation;
import com.vatit.vattattack.be.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class CalculationService {
    private SyCountryService syCountryService;
    private VtVatSchemeService vtVatSchemeService;
    private VtExpenseAgreementVatRateService vtExpenseAgreementVatRateService;
    private VtVatSchemeExpenseAgreementService vtVatSchemeExpenseAgreementService;
    private VtVatSchemeExpenseTypeService vtVatSchemeExpenseTypeService;
    private ReNoReciprocityService reNoReciprocityService;
    private VtVatSchemePeriodService vtVatSchemePeriodService;

    @Autowired
    public CalculationService(ReNoReciprocityService reNoReciprocityService, SyCountryService syCountryService,
                              VtVatSchemeService vtVatSchemeService, VtExpenseAgreementVatRateService vtExpenseAgreementVatRateService,
                              VtVatSchemeExpenseAgreementService vtVatSchemeExpenseAgreementService, VtVatSchemeExpenseTypeService vtVatSchemeExpenseTypeService,
                              VtVatSchemePeriodService vtVatSchemePeriodService) {
        this.syCountryService = syCountryService;
        this.vtVatSchemeService = vtVatSchemeService;
        this.vtExpenseAgreementVatRateService = vtExpenseAgreementVatRateService;
        this.vtVatSchemeExpenseAgreementService = vtVatSchemeExpenseAgreementService;
        this.vtVatSchemeExpenseTypeService = vtVatSchemeExpenseTypeService;
        this.reNoReciprocityService = reNoReciprocityService;
        this.vtVatSchemePeriodService = vtVatSchemePeriodService;
    }

    public ArrayList<Invoice> initialiseArpCalculation(ArrayList<Invoice> request) {
        ArrayList<VtVatScheme> vtVatSchemes = new ArrayList<>();
        ArrayList<SyCountry> countries = new ArrayList<>();
        ArrayList<VtVatSchemeExpenseAgreement> vtVatSchemeExpenseAgreements = new ArrayList<>();
        ArrayList<VtExpenseAgreementVatRate> vtExpenseAgreementVatRates = new ArrayList<>();
        ArrayList<VtVatSchemeExpenseType> vtVatSchemeExpenseTypes = new ArrayList<>();
        ArrayList<ReNoReciprocity> reNoReciprocities = new ArrayList<>();

        vtVatSchemeService.getEntities().forEach(vtVatSchemes::add);
        syCountryService.getEntities().forEach(countries::add);
        vtVatSchemeExpenseAgreementService.getEntities().forEach(vtVatSchemeExpenseAgreements::add);
        vtExpenseAgreementVatRateService.getEntities().forEach(vtExpenseAgreementVatRates::add);
        vtVatSchemeExpenseTypeService.getEntities().forEach(vtVatSchemeExpenseTypes::add);
        reNoReciprocityService.getEntities().forEach(reNoReciprocities::add);


        request.forEach(
                invoice -> invoice.setExpenseCountry(
                        countries.stream().filter(
                                syCountry -> syCountry.getCountryCode().trim().equalsIgnoreCase(invoice.getCountryCode())).collect(Collectors.toList()).get(0)));
        request.forEach(
                invoice -> invoice.setClientCountry(
                        countries.stream().filter(
                                syCountry -> syCountry.getCountryCode().trim().equalsIgnoreCase(invoice.getClientCountryCode())).collect(Collectors.toList()).get(0)));


        Set<Invoice> distinctClientCountries = request.stream().filter(distinctByKey(Invoice::getClientCountry)).collect(Collectors.toSet());
        Set<Invoice> distinctClaimCountries = request.stream().filter(distinctByKey(Invoice::getExpenseCountry)).collect(Collectors.toSet());

        Set<String> setClientCountries = new HashSet<>();
        Set<String> setClaimCountries = new HashSet<>();
        distinctClientCountries.forEach(invoice -> setClientCountries.add(invoice.getClientCountryCode().trim()));
        distinctClaimCountries.forEach(invoice -> setClaimCountries.add(invoice.getCountryCode().trim()));

        List<ReNoReciprocity> requiredNoReciprocities = reNoReciprocities
                .stream()
                .filter(reNoReciprocity -> setClientCountries.contains(reNoReciprocity.getClientCountry().getCountryCode().trim()) && setClaimCountries.contains(reNoReciprocity.getVaCountry().getCountryCode().trim())).collect(Collectors.toList());

        System.out.println("List of no reciprocity table  " + reNoReciprocities.size());
        System.out.println("List of invoice no reciprocity   " + requiredNoReciprocities.size());


        return createJobs(request, vtVatSchemes, countries, vtVatSchemeExpenseAgreements, vtExpenseAgreementVatRates, vtVatSchemeExpenseTypes, requiredNoReciprocities);
    }


    private ArrayList<Invoice> createJobs(ArrayList<Invoice> initialRequest, ArrayList<VtVatScheme> vtVatSchemes, ArrayList<SyCountry> countries, ArrayList<VtVatSchemeExpenseAgreement> vtVatSchemeExpenseAgreements, ArrayList<VtExpenseAgreementVatRate> vtExpenseAgreementVatRates, ArrayList<VtVatSchemeExpenseType> vtVatSchemeExpenseTypes, List<ReNoReciprocity> reNoReciprocities) {
        ArrayList<Invoice> newInvoices = new ArrayList<>();
        ArrayList<Future<ArrayList<Invoice>>> futureArrayList = new ArrayList<>();
        int threads = 5;//8
        int threadSize = initialRequest.size() / threads;
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 1; i <= threads; i++) {
            System.err.println("start = " + (i == 1 ? 0 : i - 1) * threadSize + " , end = " + (i != threads ? threadSize * (i == 0 ? 1 : i) : initialRequest.size()));
            ArpCalculation callable = new ArpCalculation(initialRequest.subList((i == 1 ? 0 : i - 1) * threadSize, i != threads ? threadSize * (i == 0 ? 1 : i) : initialRequest.size()), vtVatSchemes, vtVatSchemeExpenseAgreements, vtExpenseAgreementVatRates, vtVatSchemeExpenseTypes, reNoReciprocities, this);
            futureArrayList.add(executorService.submit(callable));
        }
        try {
            for (Future<ArrayList<Invoice>> arrayListFuture : futureArrayList) {
                System.out.println("size " + arrayListFuture.get().size());
                newInvoices.addAll(arrayListFuture.get());
            }
            executorService.shutdown();
            executorService.awaitTermination(60, TimeUnit.MINUTES);
            System.out.println(newInvoices.size() + "final size");
            return newInvoices;
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return newInvoices;
    }


    public ArrayList<Invoice> calculateARP(ArrayList<Invoice> request, ArrayList<VtVatScheme> vtVatSchemes, ArrayList<VtVatSchemeExpenseAgreement> vtVatSchemeExpenseAgreements, ArrayList<VtExpenseAgreementVatRate> vtExpenseAgreementVatRates, ArrayList<VtVatSchemeExpenseType> vtVatSchemeExpenseTypes, List<ReNoReciprocity> reNoReciprocities) {
        long startTime = System.currentTimeMillis();

        validateInput(request);
        validateReciprocity(request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()), reNoReciprocities);
        validateExpenseType10(request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()));
        getVatScheme(request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()), vtVatSchemes);
        getVatSchemeExpenseType(request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()), vtVatSchemeExpenseTypes);

        getRates(request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()), vtExpenseAgreementVatRates, vtVatSchemeExpenseAgreements);
        runInvoiceRules(request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()));


        request.stream().filter(invoice -> invoice.getCalculationErrors().size() == 0).collect(Collectors.toList()).forEach(invoice -> invoice.setArp(invoice.getVatSchemeVatRate() * invoice.getVatSchemeExpenseAgreementRate()));


        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.err.println("Duration = " + duration);

        return request;
    }

    private void runInvoiceRules(List<Invoice> request) {
        for (Invoice invoice : request) {

        }
    }

    private void validateExpenseType10(List<Invoice> initialRequest) {
        initialRequest.stream().filter(invoice -> invoice.getDragonId() == 10).map(invoice -> invoice.getCalculationErrors().add("Cannot calculate expense type 10."));
    }

    private void validateReciprocity(List<Invoice> initialRequest, List<ReNoReciprocity> reNoReciprocities) {
        for (Invoice invoice : initialRequest) {
            for (ReNoReciprocity reNoReciprocity : reNoReciprocities) {
                if (reNoReciprocity.getClientCountry().getCountryCode().trim().equalsIgnoreCase(invoice.getClientCountryCode())
                        && reNoReciprocity.getVaCountry().getCountryCode().trim().equalsIgnoreCase(invoice.getCountryCode())) {
                    invoice.getCalculationErrors().add("No reciprocity between claim and client country.");
                    break;
                }
            }
        }
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    private void getRates(List<Invoice> initialRequest, ArrayList<VtExpenseAgreementVatRate> vtExpenseAgreementVatRates, ArrayList<VtVatSchemeExpenseAgreement> vtVatSchemeExpenseAgreements) {
        for (Invoice invoice : initialRequest) {
            if (invoice.getVatSchemeExpenseTypeId() == null) {
                invoice.getCalculationErrors().add("Could not find expense code.");
            } else {
                Optional<VtVatSchemeExpenseAgreement> vsExpenseRate = vtVatSchemeExpenseAgreements
                        .stream()
                        .filter(vsExpenseAgreement -> vsExpenseAgreement.getVtVatSchemeExpenseType().getVatSchemeExpenseTypeId().equals(invoice.getVatSchemeExpenseTypeId()))
                        .findFirst();
                vsExpenseRate.ifPresent(vtVatSchemeExpenseAgreement -> invoice.setVatSchemeExpenseAgreementRate(vtVatSchemeExpenseAgreement.getRefundPercent().doubleValue()));
                if (!vsExpenseRate.isPresent())
                    invoice.getCalculationErrors().add("No vat scheme expense agreement captured.");
                Optional<VtExpenseAgreementVatRate> vsVatRate = vtExpenseAgreementVatRates
                        .stream()
                        .filter(vsExpenesAgreementVatRate -> vsExpenesAgreementVatRate.getVtVatSchemeExpenseType().getVatSchemeExpenseTypeId().equals(invoice.getVatSchemeExpenseTypeId()))
                        .findFirst();
                vsVatRate.ifPresent(vtExpenseAgreementVatRate -> invoice.setVatSchemeVatRate(vtExpenseAgreementVatRate.getRate().doubleValue()));
                if (!vsVatRate.isPresent())
                    invoice.getCalculationErrors().add("No vat rate captured for expense agreement.");
            }
        }
    }

    private void getVatSchemeExpenseType(List<Invoice> initialRequest, ArrayList<VtVatSchemeExpenseType> vtVatSchemeExpenseTypes) {
        for (Invoice invoice : initialRequest) {
            if (invoice.getVatSchemeID() > 0) {
                Optional<VtVatSchemeExpenseType> vsExpenseTypeId = vtVatSchemeExpenseTypes
                        .stream()
                        .filter(vsExpenseType ->
                                vsExpenseType.getVtVatScheme().getVatSchemeId().equals(invoice.getVatSchemeID())
                                        && vsExpenseType.getVtExpenseType().getExpenseTypeId().equals(invoice.getDragonId())).findFirst();
                vsExpenseTypeId.ifPresent(vtVatSchemeExpenseType -> invoice.setVatSchemeExpenseTypeId(vtVatSchemeExpenseType.getVatSchemeExpenseTypeId()));
            } else {
                invoice.getCalculationErrors().add("No vat scheme could be found.");
            }
        }
    }

    private void getVatScheme(List<Invoice> initialRequest, ArrayList<VtVatScheme> vtVatSchemes) {
        for (Invoice invoice : initialRequest) {
            if (invoice.getCountryCode().equalsIgnoreCase(invoice.getClientCountryCode())) {
                //todo 6th
                Optional<VtVatScheme> vatScheme = vtVatSchemes
                        .stream()
                        .filter(vtVatScheme -> vtVatScheme.getVtVatAuthority().getSyCountry().getCountryCode().trim().equalsIgnoreCase(invoice.getCountryCode().trim()) && vtVatScheme.getVtVatSchemeType().getVatSchemeTypeId() == 1)
                        .findFirst();
                vatScheme.ifPresent(invoice::setVtVatScheme);
                invoice.setDomesticVat(true);

            } else if (!invoice.getCountryCode().equalsIgnoreCase(invoice.getClientCountryCode()) &&
                    invoice.getClientCountry().getSyCountryGroup() != null && invoice.getExpenseCountry().getSyCountryGroup() != null) {

                Optional<VtVatScheme> vatScheme = vtVatSchemes
                        .stream()
                        .filter(o -> o.getVtVatAuthority().getSyCountry().getCountryCode().trim().equalsIgnoreCase(invoice.getCountryCode().trim()) && o.getVtVatSchemeType().getVatSchemeTypeId() == 2)
                        .findFirst();
                vatScheme.ifPresent(invoice::setVtVatScheme);
                invoice.setDomesticVat(false);

            } else if (!invoice.getCountryCode().equalsIgnoreCase(invoice.getClientCountryCode()) && invoice.getExpenseCountry().getSyCountryGroup() == null
                    || !invoice.getCountryCode().equalsIgnoreCase(invoice.getClientCountryCode()) && invoice.getClientCountry().getSyCountryGroup() == null) {

                //todo 13th
                Optional<VtVatScheme> vatScheme = vtVatSchemes
                        .stream()
                        .filter(o -> o.getVtVatAuthority().getSyCountry().getCountryCode().trim().equalsIgnoreCase(invoice.getCountryCode().trim()) && o.getVtVatSchemeType().getVatSchemeTypeId() == 3)
                        .findFirst();
                vatScheme.ifPresent(invoice::setVtVatScheme);
                invoice.setDomesticVat(false);
            }
        }
    }

    private void validateInput(List<Invoice> initialRequest) {
        for (Invoice invoice : initialRequest) {
            if (invoice.getDate() == null || invoice.getDate().isEmpty())
                invoice.getCalculationErrors().add("Transaction Dat is required.");
            if (invoice.getDate() == null || invoice.getDate().isEmpty())
                invoice.getCalculationErrors().add("Amount is required.");
            if (invoice.getDate() == null || invoice.getDate().isEmpty())
                invoice.getCalculationErrors().add("Expense Type is required.");
            if (invoice.getDate() == null || invoice.getDate().isEmpty())
                invoice.getCalculationErrors().add("Expense Country Code");
            if (invoice.getDate() == null || invoice.getDate().isEmpty())
                invoice.getCalculationErrors().add("Client  Country Code");
        }
    }
}
