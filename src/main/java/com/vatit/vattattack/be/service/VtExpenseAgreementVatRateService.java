package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtExpenseAgreementVatRate;
import com.vatit.vattattack.be.repository.VtExpenseAgreementVatRateRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtExpenseAgreementVatRateService extends CrudService<VtExpenseAgreementVatRate, String> {
    private VtExpenseAgreementVatRateRepository VtExpenseAgreementVatRateRepository;

    @Autowired
    public VtExpenseAgreementVatRateService(VtExpenseAgreementVatRateRepository VtExpenseAgreementVatRateRepository) {
        this.VtExpenseAgreementVatRateRepository = VtExpenseAgreementVatRateRepository;
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_VAT_RATES, allEntries = true)
    public VtExpenseAgreementVatRate addEntity(VtExpenseAgreementVatRate VtExpenseAgreementVatRate) {
        return VtExpenseAgreementVatRateRepository.save(VtExpenseAgreementVatRate);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_VAT_RATES, allEntries = true)
    public void deleteEntity(String primaryKey) {
        VtExpenseAgreementVatRateRepository.deleteById(primaryKey);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_VAT_RATES, allEntries = true)
    public VtExpenseAgreementVatRate updateEntity(VtExpenseAgreementVatRate VtExpenseAgreementVatRate) {
        return VtExpenseAgreementVatRateRepository.save(VtExpenseAgreementVatRate);
    }

    @Override
    public Optional<VtExpenseAgreementVatRate> getEntityByID(String primaryKey) {
        return VtExpenseAgreementVatRateRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_EXPENSE_VAT_RATES)
    public Iterable<VtExpenseAgreementVatRate> getEntities() {
        return VtExpenseAgreementVatRateRepository.findAllWithQuery();
    }
}
