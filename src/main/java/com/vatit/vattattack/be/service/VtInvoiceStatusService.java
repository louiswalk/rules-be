package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtInvoiceStatus;
import com.vatit.vattattack.be.repository.VtInvoiceStatusRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtInvoiceStatusService extends CrudService<VtInvoiceStatus, String> {

    private VtInvoiceStatusRepository vtInvoiceStatusRepository;

    @Autowired
    public VtInvoiceStatusService(VtInvoiceStatusRepository VtInvoiceStatusRepository) {
        this.vtInvoiceStatusRepository = VtInvoiceStatusRepository;
    }

    @Override
    public Optional<VtInvoiceStatus> getEntityByID(String primaryKey) {
        return vtInvoiceStatusRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_INVOICE_STATUS)
    public Iterable<VtInvoiceStatus> getEntities() {
        return vtInvoiceStatusRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_INVOICE_STATUS, allEntries = true)
    public VtInvoiceStatus updateEntity(VtInvoiceStatus entity) {
        return vtInvoiceStatusRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_INVOICE_STATUS, allEntries = true)
    public VtInvoiceStatus addEntity(VtInvoiceStatus entity) {
        return vtInvoiceStatusRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_INVOICE_STATUS, allEntries = true)
    public void deleteEntity(String primaryKey) {
        vtInvoiceStatusRepository.deleteById(primaryKey);

    }
}
