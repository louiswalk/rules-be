package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemeType;
import com.vatit.vattattack.be.repository.VtVatSchemeTypeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemeTypeService extends CrudService<VtVatSchemeType, Integer> {
    private VtVatSchemeTypeRepository vtVatSchemeTypeRepository;

    @Autowired
    public VtVatSchemeTypeService(VtVatSchemeTypeRepository vtVatSchemeTypeRepository) {
        this.vtVatSchemeTypeRepository = vtVatSchemeTypeRepository;
    }

    @Override
    public Optional<VtVatSchemeType> getEntityByID(Integer primaryKey) {
        return vtVatSchemeTypeRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_TYPE)
    public Iterable<VtVatSchemeType> getEntities() {
        return vtVatSchemeTypeRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_TYPE, allEntries = true)
    public VtVatSchemeType updateEntity(VtVatSchemeType entity) {
        return vtVatSchemeTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_TYPE, allEntries = true)
    public VtVatSchemeType addEntity(VtVatSchemeType entity) {
        return vtVatSchemeTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_TYPE, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtVatSchemeTypeRepository.deleteById(primaryKey);

    }
}
