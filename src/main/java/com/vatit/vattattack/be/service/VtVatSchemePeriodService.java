package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemePeriod;
import com.vatit.vattattack.be.repository.VtVatSchemePeriodRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemePeriodService extends CrudService<VtVatSchemePeriod, Integer> {
    private VtVatSchemePeriodRepository vtVatSchemePeriodRepository;

    @Autowired
    public VtVatSchemePeriodService(VtVatSchemePeriodRepository vtVatSchemePeriodRepository) {
        this.vtVatSchemePeriodRepository = vtVatSchemePeriodRepository;
    }

    @Override
    public Optional<VtVatSchemePeriod> getEntityByID(Integer primaryKey) {
        return vtVatSchemePeriodRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_PERIOD)
    public Iterable<VtVatSchemePeriod> getEntities() {
        return vtVatSchemePeriodRepository.findAllWithQuery();
    }

    @Cacheable(Constants.API_VAT_SCHEME_PERIOD)
    public Iterable<VtVatSchemePeriod> getInvoicePeriodInfo(Integer periodId,Integer startMonth, Integer endMonth){
        return vtVatSchemePeriodRepository.findInvoiceInfo(periodId,startMonth, endMonth);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD, allEntries = true)
    public VtVatSchemePeriod updateEntity(VtVatSchemePeriod entity) {
        return vtVatSchemePeriodRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD, allEntries = true)
    public VtVatSchemePeriod addEntity(VtVatSchemePeriod entity) {
        return vtVatSchemePeriodRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtVatSchemePeriodRepository.deleteById(primaryKey);

    }
}
