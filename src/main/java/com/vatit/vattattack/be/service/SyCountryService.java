package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.SyCountry;
import com.vatit.vattattack.be.repository.SyCountryRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SyCountryService extends CrudService<SyCountry, String> {

    private SyCountryRepository syCountryRepository;

    @Autowired
    public SyCountryService(SyCountryRepository syCountryRepository) {
        this.syCountryRepository = syCountryRepository;
    }

    @Override
    public Optional<SyCountry> getEntityByID(String primaryKey) {
        return syCountryRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_SY_COUNTRY)
    public Iterable<SyCountry> getEntities() {
        return syCountryRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_SY_COUNTRY, allEntries = true)
    public SyCountry updateEntity(SyCountry entity) {
        return syCountryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_SY_COUNTRY, allEntries = true)
    public SyCountry addEntity(SyCountry entity) {
        return syCountryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_SY_COUNTRY, allEntries = true)
    public void deleteEntity(String primaryKey) {
        syCountryRepository.deleteById(primaryKey);

    }
}
