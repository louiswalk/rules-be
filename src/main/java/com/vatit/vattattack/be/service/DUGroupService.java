package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.DuGroup;
import com.vatit.vattattack.be.repository.DuGroupsRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DUGroupService extends CrudService<DuGroup, Integer> {
    private DuGroupsRepository duGroupRepository;

    @Autowired
    public DUGroupService(DuGroupsRepository duGroupRepository) {
        this.duGroupRepository = duGroupRepository;
    }

    @Override
    @CacheEvict(value = Constants.API_DU_GROUPS, allEntries = true)
    public DuGroup addEntity(DuGroup DuGroup) {
        return duGroupRepository.save(DuGroup);
    }

    @Override
    @CacheEvict(value = Constants.API_DU_GROUPS, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        duGroupRepository.deleteById(primaryKey);
    }

    @Override
    @CacheEvict(value = Constants.API_DU_GROUPS, allEntries = true)
    public DuGroup updateEntity(DuGroup DuGroup) {
        return duGroupRepository.save(DuGroup);
    }

    @Override
    public Optional<DuGroup> getEntityByID(Integer primaryKey) {
        return duGroupRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_DU_GROUPS)
    public Iterable<DuGroup> getEntities() {
        return duGroupRepository.findAll();
    }
}
