package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtSupportingDocument;
import com.vatit.vattattack.be.repository.VtSupportingDocumentRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtSupportingDocumentService extends CrudService<VtSupportingDocument, Integer> {
    private VtSupportingDocumentRepository supportingDocumentRepository;

    @Autowired
    public VtSupportingDocumentService(VtSupportingDocumentRepository supportingDocumentRepository) {
        this.supportingDocumentRepository = supportingDocumentRepository;
    }

    @Override
    @CacheEvict(value = Constants.API_SUPPORTING_DOCUMENTS, allEntries = true)
    public VtSupportingDocument addEntity(VtSupportingDocument VtSupportingDocument) {
        return supportingDocumentRepository.save(VtSupportingDocument);
    }

    @Override
    @CacheEvict(value = Constants.API_SUPPORTING_DOCUMENTS, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        supportingDocumentRepository.deleteById(primaryKey);
    }

    @Override
    @CacheEvict(value = Constants.API_SUPPORTING_DOCUMENTS, allEntries = true)
    public VtSupportingDocument updateEntity(VtSupportingDocument VtSupportingDocument) {
        return supportingDocumentRepository.save(VtSupportingDocument);
    }

    @Override
    public Optional<VtSupportingDocument> getEntityByID(Integer primaryKey) {
        return supportingDocumentRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_SUPPORTING_DOCUMENTS)
    public Iterable<VtSupportingDocument> getEntities() {
        return supportingDocumentRepository.findAll();
    }
}
