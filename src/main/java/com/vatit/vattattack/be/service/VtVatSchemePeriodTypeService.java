package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemePeriodType;
import com.vatit.vattattack.be.repository.VtVatSchemePeriodTypeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemePeriodTypeService extends CrudService<VtVatSchemePeriodType, Integer> {
    private VtVatSchemePeriodTypeRepository VtVatSchemePeriodTypeRepository;

    @Autowired
    public VtVatSchemePeriodTypeService(VtVatSchemePeriodTypeRepository VtVatSchemePeriodTypeRepository) {
        this.VtVatSchemePeriodTypeRepository = VtVatSchemePeriodTypeRepository;
    }

    @Override
    public Optional<VtVatSchemePeriodType> getEntityByID(Integer primaryKey) {
        return VtVatSchemePeriodTypeRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_PERIOD_TYPE)
    public Iterable<VtVatSchemePeriodType> getEntities() {
        return VtVatSchemePeriodTypeRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD_TYPE, allEntries = true)
    public VtVatSchemePeriodType updateEntity(VtVatSchemePeriodType entity) {
        return VtVatSchemePeriodTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD_TYPE, allEntries = true)
    public VtVatSchemePeriodType addEntity(VtVatSchemePeriodType entity) {
        return VtVatSchemePeriodTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD_TYPE, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        VtVatSchemePeriodTypeRepository.deleteById(primaryKey);

    }
}
