package com.vatit.vattattack.be.service;

import java.util.Optional;

public abstract class CrudService<T, V> {
    abstract Iterable<T> getEntities();

    abstract T updateEntity(T entity);

    abstract T addEntity(T entity);

    abstract void deleteEntity(V primaryKey);

    abstract Optional<T> getEntityByID(V primaryKey);
}
