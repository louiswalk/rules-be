package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemePeriodDescription;
import com.vatit.vattattack.be.repository.VtVatSchemePeriodDescriptionRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemePeriodDescriptionService extends CrudService<VtVatSchemePeriodDescription, Integer> {
    private VtVatSchemePeriodDescriptionRepository VtVatSchemePeriodDescriptionRepository;

    @Autowired
    public VtVatSchemePeriodDescriptionService(VtVatSchemePeriodDescriptionRepository VtVatSchemePeriodDescriptionRepository) {
        this.VtVatSchemePeriodDescriptionRepository = VtVatSchemePeriodDescriptionRepository;
    }

    @Override
    public Optional<VtVatSchemePeriodDescription> getEntityByID(Integer primaryKey) {
        return VtVatSchemePeriodDescriptionRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION)
    public Iterable<VtVatSchemePeriodDescription> getEntities() {
        return VtVatSchemePeriodDescriptionRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION, allEntries = true)
    public VtVatSchemePeriodDescription updateEntity(VtVatSchemePeriodDescription entity) {
        return VtVatSchemePeriodDescriptionRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION, allEntries = true)
    public VtVatSchemePeriodDescription addEntity(VtVatSchemePeriodDescription entity) {
        return VtVatSchemePeriodDescriptionRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_PERIOD_DESCRIPTION, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        VtVatSchemePeriodDescriptionRepository.deleteById(primaryKey);

    }
}
