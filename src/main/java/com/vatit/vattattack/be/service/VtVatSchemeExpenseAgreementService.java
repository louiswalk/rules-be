package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemeExpenseAgreement;
import com.vatit.vattattack.be.repository.VtVatSchemeExpenseAgreementRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemeExpenseAgreementService extends CrudService<VtVatSchemeExpenseAgreement, String> {
    private VtVatSchemeExpenseAgreementRepository vtVatSchemeExpenseAgreementRepository;

    @Autowired
    public VtVatSchemeExpenseAgreementService(VtVatSchemeExpenseAgreementRepository vtVatSchemeExpenseAgreementRepository) {
        this.vtVatSchemeExpenseAgreementRepository = vtVatSchemeExpenseAgreementRepository;
    }

    @Override
    public Optional<VtVatSchemeExpenseAgreement> getEntityByID(String primaryKey) {
        return vtVatSchemeExpenseAgreementRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT)
    public Iterable<VtVatSchemeExpenseAgreement> getEntities() {
        return vtVatSchemeExpenseAgreementRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT, allEntries = true)
    public VtVatSchemeExpenseAgreement updateEntity(VtVatSchemeExpenseAgreement entity) {
        return vtVatSchemeExpenseAgreementRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT, allEntries = true)
    public VtVatSchemeExpenseAgreement addEntity(VtVatSchemeExpenseAgreement entity) {
        return vtVatSchemeExpenseAgreementRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_EXPENSE_AGREEMENT, allEntries = true)
    public void deleteEntity(String primaryKey) {
        vtVatSchemeExpenseAgreementRepository.deleteById(primaryKey);

    }
}
