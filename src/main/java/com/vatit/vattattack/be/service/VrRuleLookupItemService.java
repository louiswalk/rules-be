package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VrRuleLookupItem;
import com.vatit.vattattack.be.repository.VrRuleLookupItemRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VrRuleLookupItemService extends CrudService<VrRuleLookupItem, String> {

    private VrRuleLookupItemRepository VrRuleLookupItemRepository;

    @Autowired
    public VrRuleLookupItemService(VrRuleLookupItemRepository VrRuleLookupItemRepository) {
        this.VrRuleLookupItemRepository = VrRuleLookupItemRepository;
    }

    @Override
    public Optional<VrRuleLookupItem> getEntityByID(String primaryKey) {
        return VrRuleLookupItemRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_RULE_LOOKUP_ITEM)
    public Iterable<VrRuleLookupItem> getEntities() {
        return VrRuleLookupItemRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_RULE_LOOKUP_ITEM, allEntries = true)
    public VrRuleLookupItem updateEntity(VrRuleLookupItem entity) {
        return VrRuleLookupItemRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_RULE_LOOKUP_ITEM, allEntries = true)
    public VrRuleLookupItem addEntity(VrRuleLookupItem entity) {
        return VrRuleLookupItemRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_RULE_LOOKUP_ITEM, allEntries = true)
    public void deleteEntity(String primaryKey) {
        VrRuleLookupItemRepository.deleteById(primaryKey);

    }
}
