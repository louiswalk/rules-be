package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtExpenseTypeCategory;
import com.vatit.vattattack.be.repository.VtExpenseTypeCategoryRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtExpenseTypeCategoryService extends CrudService<VtExpenseTypeCategory, Integer> {
    private VtExpenseTypeCategoryRepository vtExpenseTypeCategoryRepository;

    @Autowired
    public VtExpenseTypeCategoryService(VtExpenseTypeCategoryRepository vtExpenseTypeCategoryRepository) {
        this.vtExpenseTypeCategoryRepository = vtExpenseTypeCategoryRepository;
    }

    @Override
    public Optional<VtExpenseTypeCategory> getEntityByID(Integer primaryKey) {
        return vtExpenseTypeCategoryRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_EXPENSE_TYPE_CATEGORIES)
    public Iterable<VtExpenseTypeCategory> getEntities() {
        return vtExpenseTypeCategoryRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_CATEGORIES, allEntries = true)
    public VtExpenseTypeCategory updateEntity(VtExpenseTypeCategory entity) {
        return vtExpenseTypeCategoryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_CATEGORIES, allEntries = true)
    public VtExpenseTypeCategory addEntity(VtExpenseTypeCategory entity) {
        return vtExpenseTypeCategoryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_CATEGORIES, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtExpenseTypeCategoryRepository.deleteById(primaryKey);

    }
}
