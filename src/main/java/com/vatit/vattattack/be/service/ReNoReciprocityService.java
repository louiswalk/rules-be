package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.ReNoReciprocity;
import com.vatit.vattattack.be.repository.ReNoReciprocityRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReNoReciprocityService extends CrudService<ReNoReciprocity, Integer> {
    private ReNoReciprocityRepository reNoReciprocityRepository;

    @Autowired
    public ReNoReciprocityService(ReNoReciprocityRepository reNoReciprocityRepository) {
        this.reNoReciprocityRepository = reNoReciprocityRepository;
    }

    @Override
    @CacheEvict(value = Constants.API_RE_NO_RECIPROCITY, allEntries = true)
    public ReNoReciprocity addEntity(ReNoReciprocity ReNoReciprocity) {
        return reNoReciprocityRepository.save(ReNoReciprocity);
    }

    @Override
    @CacheEvict(value = Constants.API_RE_NO_RECIPROCITY, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        reNoReciprocityRepository.deleteById(primaryKey);
    }

    @Override
    @CacheEvict(value = Constants.API_RE_NO_RECIPROCITY, allEntries = true)
    public ReNoReciprocity updateEntity(ReNoReciprocity ReNoReciprocity) {
        return reNoReciprocityRepository.save(ReNoReciprocity);
    }

    @Override
    public Optional<ReNoReciprocity> getEntityByID(Integer primaryKey) {
        return reNoReciprocityRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_RE_NO_RECIPROCITY)
    public Iterable<ReNoReciprocity> getEntities() {
        return reNoReciprocityRepository.findAllWithQuery();
    }
}
