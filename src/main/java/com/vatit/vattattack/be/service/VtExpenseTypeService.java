package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtExpenseType;
import com.vatit.vattattack.be.repository.VtExpenseTypeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtExpenseTypeService extends CrudService<VtExpenseType, Integer> {
    private VtExpenseTypeRepository vtExpenseTypeRepository;

    @Autowired
    public VtExpenseTypeService(VtExpenseTypeRepository vtExpenseTypeRepository) {
        this.vtExpenseTypeRepository = vtExpenseTypeRepository;
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPES, allEntries = true)
    public VtExpenseType addEntity(VtExpenseType VtExpenseType) {
        return vtExpenseTypeRepository.save(VtExpenseType);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPES, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtExpenseTypeRepository.deleteById(primaryKey);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPES, allEntries = true)
    public VtExpenseType updateEntity(VtExpenseType VtExpenseType) {
        return vtExpenseTypeRepository.save(VtExpenseType);
    }

    @Override
    public Optional<VtExpenseType> getEntityByID(Integer primaryKey) {
        return vtExpenseTypeRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_EXPENSE_TYPES)
    public Iterable<VtExpenseType> getEntities() {
        return vtExpenseTypeRepository.findAllWithQuery();
    }
}
