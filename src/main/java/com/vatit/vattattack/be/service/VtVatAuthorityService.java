package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.CiEntity;
import com.vatit.vattattack.be.domain.VtVatAuthority;
import com.vatit.vattattack.be.repository.CiEntityRepository;
import com.vatit.vattattack.be.repository.VtVatAuthorityRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class VtVatAuthorityService extends CrudService<VtVatAuthority, String> {
    private CiEntityRepository ciEntityRepository;
    private VtVatAuthorityRepository vtVatAuthorityRepository;

    @Autowired
    public VtVatAuthorityService(CiEntityRepository ciEntityRepository, VtVatAuthorityRepository vtVatAuthorityRepository) {
        this.ciEntityRepository = ciEntityRepository;
        this.vtVatAuthorityRepository = vtVatAuthorityRepository;
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_AUTHORITIES, allEntries = true)
    @Transactional
    public VtVatAuthority addEntity(VtVatAuthority vtVatAuthority) {
        var ciEntity = new CiEntity(vtVatAuthority.getEntityCodeVatAuthority(), Constants.EntityType_Other, vtVatAuthority.getName(), Constants.FALSE_CHAR, Constants.TABLE_VatAuthority);
        ciEntityRepository.save(ciEntity);
        return vtVatAuthorityRepository.save(vtVatAuthority);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_AUTHORITIES, allEntries = true)
    @Transactional
    public void deleteEntity(String primaryKey) {
        ciEntityRepository.deleteById(primaryKey);
        vtVatAuthorityRepository.deleteById(primaryKey);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_AUTHORITIES, allEntries = true)
    public VtVatAuthority updateEntity(VtVatAuthority vtVatAuthority) {
        return vtVatAuthorityRepository.save(vtVatAuthority);
    }

    @Override
    public Optional<VtVatAuthority> getEntityByID(String primaryKey) {
        return vtVatAuthorityRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_AUTHORITIES)
    public Iterable<VtVatAuthority> getEntities() {
        return vtVatAuthorityRepository.findAllWithQuery();
    }
}
