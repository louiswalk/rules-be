package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatScheme;
import com.vatit.vattattack.be.repository.VtVatSchemeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemeService extends CrudService<VtVatScheme, Integer> {
    private VtVatSchemeRepository vtVatSchemeTypeRepository;

    @Autowired
    public VtVatSchemeService(VtVatSchemeRepository vtVatSchemeTypeRepository) {
        this.vtVatSchemeTypeRepository = vtVatSchemeTypeRepository;
    }

    @Override
    public Optional<VtVatScheme> getEntityByID(Integer primaryKey) {
        return vtVatSchemeTypeRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(value = Constants.API_VAT_SCHEME)
    public Iterable<VtVatScheme> getEntities() {
        return vtVatSchemeTypeRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME, allEntries = true)
    public VtVatScheme updateEntity(VtVatScheme entity) {
        return vtVatSchemeTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME, allEntries = true)
    public VtVatScheme addEntity(VtVatScheme entity) {
        return vtVatSchemeTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtVatSchemeTypeRepository.deleteById(primaryKey);
    }
}
