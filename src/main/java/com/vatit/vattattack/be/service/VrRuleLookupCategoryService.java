package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VrRuleLookupCategory;
import com.vatit.vattattack.be.repository.VrRuleLookupCategoryRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VrRuleLookupCategoryService extends CrudService<VrRuleLookupCategory, String> {

    private VrRuleLookupCategoryRepository VrRuleLookupCategoryRepository;

    @Autowired
    public VrRuleLookupCategoryService(VrRuleLookupCategoryRepository VrRuleLookupCategoryRepository) {
        this.VrRuleLookupCategoryRepository = VrRuleLookupCategoryRepository;
    }

    @Override
    public Optional<VrRuleLookupCategory> getEntityByID(String primaryKey) {
        return VrRuleLookupCategoryRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_RULES_LOOKUP_CATEGORY)
    public Iterable<VrRuleLookupCategory> getEntities() {
        return VrRuleLookupCategoryRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_RULES_LOOKUP_CATEGORY, allEntries = true)
    public VrRuleLookupCategory updateEntity(VrRuleLookupCategory entity) {
        return VrRuleLookupCategoryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_RULES_LOOKUP_CATEGORY, allEntries = true)
    public VrRuleLookupCategory addEntity(VrRuleLookupCategory entity) {
        return VrRuleLookupCategoryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_RULES_LOOKUP_CATEGORY, allEntries = true)
    public void deleteEntity(String primaryKey) {
        VrRuleLookupCategoryRepository.deleteById(primaryKey);

    }
}
