package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemeExpenseType;
import com.vatit.vattattack.be.repository.VtVatSchemeExpenseTypeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemeExpenseTypeService extends CrudService<VtVatSchemeExpenseType, String> {
    private VtVatSchemeExpenseTypeRepository vtVatSchemeExpenseTypeRepository;

    @Autowired
    public VtVatSchemeExpenseTypeService(VtVatSchemeExpenseTypeRepository VtVatSchemeExpenseTypeRepository) {
        this.vtVatSchemeExpenseTypeRepository = VtVatSchemeExpenseTypeRepository;
    }

    @Override
    public Optional<VtVatSchemeExpenseType> getEntityByID(String primaryKey) {
        return vtVatSchemeExpenseTypeRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_EXPENSE_TYPE)
    public Iterable<VtVatSchemeExpenseType> getEntities() {
        return vtVatSchemeExpenseTypeRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_EXPENSE_TYPE, allEntries = true)
    public VtVatSchemeExpenseType updateEntity(VtVatSchemeExpenseType entity) {
        return vtVatSchemeExpenseTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_EXPENSE_TYPE, allEntries = true)
    public VtVatSchemeExpenseType addEntity(VtVatSchemeExpenseType entity) {
        return vtVatSchemeExpenseTypeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_EXPENSE_TYPE, allEntries = true)
    public void deleteEntity(String primaryKey) {
        vtVatSchemeExpenseTypeRepository.deleteById(primaryKey);

    }
}
