package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtExpenseTypeSubCode;
import com.vatit.vattattack.be.repository.VtExpenseTypeSubCodeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtExpenseTypeSubCodeService extends CrudService<VtExpenseTypeSubCode, Integer> {
    private VtExpenseTypeSubCodeRepository vtExpenseTypeSubCodeRepository;

    @Autowired
    public VtExpenseTypeSubCodeService(VtExpenseTypeSubCodeRepository vtExpenseTypeSubCodeRepository) {
        this.vtExpenseTypeSubCodeRepository = vtExpenseTypeSubCodeRepository;
    }

    @Override
    public Optional<VtExpenseTypeSubCode> getEntityByID(Integer primaryKey) {
        return vtExpenseTypeSubCodeRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_EXPENSE_TYPE_SUB_CODES)
    public Iterable<VtExpenseTypeSubCode> getEntities() {
        return vtExpenseTypeSubCodeRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_SUB_CODES, allEntries = true)
    public VtExpenseTypeSubCode updateEntity(VtExpenseTypeSubCode entity) {
        return vtExpenseTypeSubCodeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_SUB_CODES, allEntries = true)
    public VtExpenseTypeSubCode addEntity(VtExpenseTypeSubCode entity) {
        return vtExpenseTypeSubCodeRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_SUB_CODES, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtExpenseTypeSubCodeRepository.deleteById(primaryKey);

    }
}
