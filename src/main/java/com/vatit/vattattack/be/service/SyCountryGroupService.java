package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.SyCountryGroup;
import com.vatit.vattattack.be.repository.SyCountryGroupRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SyCountryGroupService extends CrudService<SyCountryGroup, String> {

    private SyCountryGroupRepository syCountryGroupRepository;

    @Autowired
    public SyCountryGroupService(SyCountryGroupRepository SyCountryGroupCategoryRepository) {
        this.syCountryGroupRepository = SyCountryGroupCategoryRepository;
    }

    @Override
    public Optional<SyCountryGroup> getEntityByID(String primaryKey) {
        return syCountryGroupRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_SY_COUNTRY_GROUP)
    public Iterable<SyCountryGroup> getEntities() {
        return syCountryGroupRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_SY_COUNTRY_GROUP, allEntries = true)
    public SyCountryGroup updateEntity(SyCountryGroup entity) {
        return syCountryGroupRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_SY_COUNTRY_GROUP, allEntries = true)
    public SyCountryGroup addEntity(SyCountryGroup entity) {
        return syCountryGroupRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_SY_COUNTRY_GROUP, allEntries = true)
    public void deleteEntity(String primaryKey) {
        syCountryGroupRepository.deleteById(primaryKey);

    }
}
