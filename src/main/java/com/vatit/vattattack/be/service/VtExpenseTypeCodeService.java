package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtExpenseTypeCode;
import com.vatit.vattattack.be.repository.VtExpenseTypeCodeRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtExpenseTypeCodeService extends CrudService<VtExpenseTypeCode, Integer> {
    private VtExpenseTypeCodeRepository VtExpenseTypeCodeCategoryRepository;

    @Autowired
    public VtExpenseTypeCodeService(VtExpenseTypeCodeRepository VtExpenseTypeCodeCategoryRepository) {
        this.VtExpenseTypeCodeCategoryRepository = VtExpenseTypeCodeCategoryRepository;
    }

    @Override
    public Optional<VtExpenseTypeCode> getEntityByID(Integer primaryKey) {
        return VtExpenseTypeCodeCategoryRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_EXPENSE_TYPE_CODES)
    public Iterable<VtExpenseTypeCode> getEntities() {
        return VtExpenseTypeCodeCategoryRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_CODES, allEntries = true)
    public VtExpenseTypeCode updateEntity(VtExpenseTypeCode entity) {
        return VtExpenseTypeCodeCategoryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_CODES, allEntries = true)
    public VtExpenseTypeCode addEntity(VtExpenseTypeCode entity) {
        return VtExpenseTypeCodeCategoryRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_EXPENSE_TYPE_CODES, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        VtExpenseTypeCodeCategoryRepository.deleteById(primaryKey);

    }
}
