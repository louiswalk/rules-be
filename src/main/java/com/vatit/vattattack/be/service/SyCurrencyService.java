package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.SyCurrency;
import com.vatit.vattattack.be.repository.SyCurrencyRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SyCurrencyService extends CrudService<SyCurrency, String> {

    private SyCurrencyRepository syCurrencyRepository;

    @Autowired
    public SyCurrencyService(SyCurrencyRepository VtExpenseTypeCodeCategoryRepository) {
        this.syCurrencyRepository = VtExpenseTypeCodeCategoryRepository;
    }

    @Override
    public Optional<SyCurrency> getEntityByID(String primaryKey) {
        return syCurrencyRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_SY_CURRENCY)
    public Iterable<SyCurrency> getEntities() {
        return syCurrencyRepository.findAll();
    }

    @Override
    @CacheEvict(value = Constants.API_SY_CURRENCY, allEntries = true)
    public SyCurrency updateEntity(SyCurrency entity) {
        return syCurrencyRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_SY_CURRENCY, allEntries = true)
    public SyCurrency addEntity(SyCurrency entity) {
        return syCurrencyRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_SY_CURRENCY, allEntries = true)
    public void deleteEntity(String primaryKey) {
        syCurrencyRepository.deleteById(primaryKey);

    }
}
