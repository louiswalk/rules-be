package com.vatit.vattattack.be.service;

import com.vatit.vattattack.be.domain.VtVatSchemeSupportingDocument;
import com.vatit.vattattack.be.repository.VtVatSchemeSupportingDocumentRepository;
import com.vatit.vattattack.be.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VtVatSchemeSupportingDocumentService extends CrudService<VtVatSchemeSupportingDocument, Integer> {
    private VtVatSchemeSupportingDocumentRepository vtVatSchemeSupportingDocumentRepository;

    @Autowired
    public VtVatSchemeSupportingDocumentService(VtVatSchemeSupportingDocumentRepository vtVatSchemeSupportingDocumentRepository) {
        this.vtVatSchemeSupportingDocumentRepository = vtVatSchemeSupportingDocumentRepository;
    }

    @Override
    public Optional<VtVatSchemeSupportingDocument> getEntityByID(Integer primaryKey) {
        return vtVatSchemeSupportingDocumentRepository.findById(primaryKey);
    }

    @Override
    @Cacheable(Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT)
    public Iterable<VtVatSchemeSupportingDocument> getEntities() {
        return vtVatSchemeSupportingDocumentRepository.findAllWithQuery();
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT_PK, allEntries = true)
    public VtVatSchemeSupportingDocument updateEntity(VtVatSchemeSupportingDocument entity) {
        return vtVatSchemeSupportingDocumentRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT, allEntries = true)
    public VtVatSchemeSupportingDocument addEntity(VtVatSchemeSupportingDocument entity) {
        return vtVatSchemeSupportingDocumentRepository.save(entity);
    }

    @Override
    @CacheEvict(value = Constants.API_VAT_SCHEME_SUPPORTING_DOCUMENT_PK, allEntries = true)
    public void deleteEntity(Integer primaryKey) {
        vtVatSchemeSupportingDocumentRepository.deleteById(primaryKey);

    }
}
